# README #

Chalice API - the backend service for chalicedungeonglyphs.com.

### What is this repository for? ###

* it's the backend service for chalicedungeonglyphs.com.  (deja vu!)

### How do I get set up? ###

* Clone repo.
* Run a MySQL server, and execute everything in the scripts/ directory.
* create a json file at ./secret/dbinfo.json
* make sure that file contains user, password and database properties
* `npm install`
* adjust host/port as needed.
* `node app`
* server can then be hit with postman or similar REST client.
* install chalice-web (different repo) if you want the actual website.

### Contribution guidelines ###

* create a pull request and I will take a look.  

### Who do I talk to? ###

* Damon Floyd <dfloyd@cowtao.com> -or-
* /u/Monkrocker on reddit.  (I only post in /r/Bloodborne)
