CREATE DATABASE  IF NOT EXISTS `tss_chalice` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tss_chalice`;
-- MySQL dump 10.13  Distrib 5.6.13, for osx10.6 (i386)
--
-- Host: localhost    Database: tss_chalice
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `FeatureTypes`
--

DROP TABLE IF EXISTS `FeatureTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FeatureTypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FeatureTypes`
--

LOCK TABLES `FeatureTypes` WRITE;
/*!40000 ALTER TABLE `FeatureTypes` DISABLE KEYS */;
INSERT INTO `FeatureTypes` VALUES (1,'Weapon'),(2,'Blood Gem'),(3,'Upgrade Material'),(4,'Ritual Material'),(5,'Caryll Rune'),(6,'Special');
/*!40000 ALTER TABLE `FeatureTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Features`
--

DROP TABLE IF EXISTS `Features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `featureTypeId` int(11) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fkFeaturesTypeId_idx` (`featureTypeId`),
  CONSTRAINT `fkFeaturesTypeId` FOREIGN KEY (`featureTypeId`) REFERENCES `FeatureTypes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Features`
--

LOCK TABLES `Features` WRITE;
/*!40000 ALTER TABLE `Features` DISABLE KEYS */;
INSERT INTO `Features` VALUES (1,1,'Lost Saw Cleaver'),(2,1,'Lost Saw Spear'),(3,1,'Lost Hunter Axe'),(4,1,'Lost Threaded Cane'),(5,1,'Lost Burial Blade'),(6,1,'Lost Blades of Mercy'),(7,1,'Lost Kirkhammer'),(8,1,'Ludwig\'s Lost Holy Blade'),(9,1,'Lost Reiterpallasch'),(10,1,'Lost Chikage'),(11,1,'Lost Rifle Spear'),(12,1,'Lost Stake Driver'),(13,1,'Lost Tonitrus'),(14,1,'Logarius\' Lost Wheel'),(15,1,'Lost Beast Claws'),(16,1,'Uncanny Saw Cleaver'),(17,1,'Uncanny Saw Spear'),(18,1,'Uncanny Hunter Axe'),(19,1,'Uncanny Threaded Cane'),(20,1,'Uncanny Burial Blade'),(21,1,'Uncanny Blades of Mercy'),(22,1,'Uncanny Kirkhammer'),(23,1,'Ludwig\'s Uncanny Holy Blade'),(24,1,'Uncanny Reiterpallasch'),(25,1,'Uncanny Chikage'),(26,1,'Uncanny Rifle Spear'),(27,1,'Uncanny Stake Driver'),(28,1,'Uncanny Tonitrus'),(29,1,'Logarius\' Uncanny Wheel'),(30,1,'Uncanny Beast Claws'),(31,3,'Blood Stone Rock'),(32,4,'Blooming Coldblood Flower'),(33,4,'Sage\'s Hair'),(40,1,'Beast Claw'),(41,4,'Arcane Haze'),(42,4,'Pearl Slug'),(43,4,'Red Jelly'),(44,4,'Yellow Backbone'),(45,4,'Sage\'s Wrist'),(47,4,'Coldblood Flowerbud'),(48,4,'Coldblood Flower Bulb'),(49,4,'Bloodshot Eyeball'),(50,4,'Inflicted Organ'),(51,4,'Bastard of Loran'),(52,3,'Blood Stone Chunk'),(53,4,'Living String'),(54,5,'Anticlockwise Metamorphosis'),(55,5,'Arcane Lake'),(56,5,'Beast'),(57,5,'Blood Rapture'),(58,5,'Clawmark'),(59,5,'Clear Deep Sea'),(60,5,'Clockwise Metamorphosis'),(61,5,'Communion'),(62,5,'Deep Sea'),(63,5,'Dissipating Lake'),(64,5,'Eye'),(65,5,'Fading Lake'),(66,5,'Formless Odeon'),(67,5,'Great Deep Sea'),(68,5,'Great Lake'),(69,5,'Heir'),(70,5,'Lake'),(71,5,'Moon'),(72,5,'Oedon Writhe'),(73,5,'Stunning Deep Sea'),(75,6,'Ring of Betrothal');
/*!40000 ALTER TABLE `Features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Offerings`
--

DROP TABLE IF EXISTS `Offerings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Offerings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Offerings`
--

LOCK TABLES `Offerings` WRITE;
/*!40000 ALTER TABLE `Offerings` DISABLE KEYS */;
INSERT INTO `Offerings` VALUES (1,'Fetid','Stronger enemies, more Blood Echoes for killing, and better drops.'),(2,'Rotted','Chance of Eye Collector, Tomb Prospector or Labyrinth Ritekeeper appearing.  Increased chance of powerful Blood Gems.'),(3,'Cursed','HP cut in half.  20% more Blood Echoes for enemies killed.  Higher chance of Rare Item drops.'),(4,'Sinister Bell','Bellringers appear which summon other players for PvP.  NPC Hunters are summoned if playing offline.');
/*!40000 ALTER TABLE `Offerings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChaliceMaterials`
--

DROP TABLE IF EXISTS `ChaliceMaterials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChaliceMaterials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chaliceId` int(11) DEFAULT '1',
  `materialId` int(11) DEFAULT '1',
  `quantity` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fkChaliceId_idx` (`chaliceId`),
  KEY `fkMaterialId_idx` (`materialId`),
  CONSTRAINT `fkChaliceId` FOREIGN KEY (`chaliceId`) REFERENCES `Chalices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkMaterialId` FOREIGN KEY (`materialId`) REFERENCES `Materials` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChaliceMaterials`
--

LOCK TABLES `ChaliceMaterials` WRITE;
/*!40000 ALTER TABLE `ChaliceMaterials` DISABLE KEYS */;
INSERT INTO `ChaliceMaterials` VALUES (1,1,1,1000),(2,1,2,2),(3,2,1,1000),(4,2,2,2),(5,2,7,3),(6,3,1,1800),(7,3,3,6),(8,4,1,1800),(9,4,3,8),(10,4,8,6),(11,5,1,3200),(12,5,4,9),(13,6,1,3200),(14,6,4,12),(15,6,9,8),(16,7,1,3200),(17,7,4,12),(18,7,9,8),(19,8,1,5500),(20,8,5,9),(21,8,12,2),(22,8,13,22),(23,9,1,5500),(24,9,5,10),(25,9,10,6),(26,10,1,11500),(27,10,6,9),(28,10,14,4),(29,10,13,25),(30,10,15,1),(31,11,1,11500),(32,11,11,15),(33,11,14,2),(34,11,6,15),(35,12,1,11500),(36,12,11,15),(37,12,14,2),(38,12,6,15),(39,13,1,1800),(40,13,3,3),(41,13,16,3),(42,14,1,1800),(43,14,3,8),(44,14,16,3),(45,14,8,10),(46,15,1,3200),(47,15,4,6),(48,15,16,6),(49,16,1,3200),(50,16,4,12),(51,16,16,6),(52,16,9,3),(53,17,1,3200),(54,17,4,12),(55,17,16,6),(56,17,9,3),(57,18,1,5500),(58,18,5,9),(59,18,17,4),(60,19,1,5500),(61,19,5,14),(62,19,18,3),(63,19,10,3),(64,20,1,11500),(65,20,6,9),(66,20,19,4),(67,21,1,11500),(68,21,6,13),(69,21,19,4),(70,21,11,5),(71,21,14,1),(72,22,1,11500),(73,22,6,13),(74,22,19,4),(75,22,11,5),(76,23,1,11500),(77,23,6,9),(78,23,20,3),(79,23,13,25),(80,24,1,11500),(81,24,6,13),(82,24,20,3),(83,24,14,1),(84,24,11,5),(85,25,1,11500),(86,25,6,13),(87,25,20,3),(88,25,11,5),(89,25,14,1);
/*!40000 ALTER TABLE `ChaliceMaterials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChaliceOfferings`
--

DROP TABLE IF EXISTS `ChaliceOfferings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChaliceOfferings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chaliceId` int(11) DEFAULT '1',
  `offeringId` int(11) DEFAULT '1',
  `materialId` int(11) DEFAULT '1',
  `quantity` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fkChaliceId_idx` (`chaliceId`),
  KEY `fkOfferingId_idx` (`offeringId`),
  KEY `fkMaterialId_idx` (`materialId`),
  KEY `fkOfferingsChaliceId_idx` (`chaliceId`),
  KEY `fkOfferingsOfferingId_idx` (`offeringId`),
  KEY `fkOfferingsMaterialId_idx` (`materialId`),
  CONSTRAINT `fkOfferingsChaliceId` FOREIGN KEY (`chaliceId`) REFERENCES `Chalices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkOfferingsMaterialId` FOREIGN KEY (`materialId`) REFERENCES `Materials` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkOfferingsOfferingId` FOREIGN KEY (`offeringId`) REFERENCES `Offerings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChaliceOfferings`
--

LOCK TABLES `ChaliceOfferings` WRITE;
/*!40000 ALTER TABLE `ChaliceOfferings` DISABLE KEYS */;
INSERT INTO `ChaliceOfferings` VALUES (1,4,1,21,2),(2,6,1,21,4),(3,6,2,22,3),(4,7,4,13,19),(5,9,1,23,3),(6,9,2,22,5),(7,9,3,12,3),(8,11,1,23,6),(9,11,2,24,6),(10,11,3,12,3),(11,12,4,13,32),(12,14,1,21,2),(13,16,1,21,4),(14,16,2,22,3),(15,17,4,13,19),(16,19,1,23,3),(17,19,2,22,5),(18,21,1,23,6),(19,21,2,24,6),(20,21,3,12,3),(21,22,4,13,32),(22,25,4,13,32),(23,24,1,23,6),(24,24,2,24,6),(25,24,3,12,3);
/*!40000 ALTER TABLE `ChaliceOfferings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Materials`
--

DROP TABLE IF EXISTS `Materials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Materials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Materials`
--

LOCK TABLES `Materials` WRITE;
/*!40000 ALTER TABLE `Materials` DISABLE KEYS */;
INSERT INTO `Materials` VALUES (1,'Blood Echoes'),(2,'Ritual Blood (1)'),(3,'Ritual Blood (2)'),(4,'Ritual Blood (3)'),(5,'Ritual Blood (4)'),(6,'Ritual Blood (5)'),(7,'Tomb Mold (1)'),(8,'Tomb Mold (2)'),(9,'Tomb Mold (3)'),(10,'Tomb Mold (4)'),(11,'Tomb Mold (5)'),(12,'Bastard of Loran'),(13,'Arcane Haze'),(14,'Red Jelly'),(15,'Living String'),(16,'Bloodshot Eyeball'),(17,'Coldblood Flowerbud'),(18,'Coldblood Flower Bulb'),(19,'Blooming Coldblood Flower'),(20,'Pearl Slug'),(21,'Sage\'s Wrist'),(22,'Inflicted Organ'),(23,'Sage\'s Hair'),(24,'Yellow Backbone');
/*!40000 ALTER TABLE `Materials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Bosses`
--

DROP TABLE IF EXISTS `Bosses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bosses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Bosses`
--

LOCK TABLES `Bosses` WRITE;
/*!40000 ALTER TABLE `Bosses` DISABLE KEYS */;
INSERT INTO `Bosses` VALUES (1,'Abhorrent Beast'),(2,'Beast-Possessed Soul'),(3,'Bloodletting Beast'),(4,'Brainsucker'),(5,'Forgotten Madman'),(6,'Keeper Of The Old Lords'),(7,'Loran Silverbeast'),(8,'Loran Darkbeast'),(9,'Maneater Boar'),(10,'Merciless Watcher'),(11,'Pthumerian Descendant'),(12,'Pthumerian Elder'),(13,'Undead Giant'),(14,'Amygdala'),(15,'Blood-Starved Beast'),(16,'Celestial Emissary'),(17,'Ebrietas'),(18,'Rom, The Vacuous Spider');
/*!40000 ALTER TABLE `Bosses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Submitters`
--

DROP TABLE IF EXISTS `Submitters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Submitters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Submitters`
--

LOCK TABLES `Submitters` WRITE;
/*!40000 ALTER TABLE `Submitters` DISABLE KEYS */;
INSERT INTO `Submitters` VALUES (1,'Spreadsheet'),(2,'JovialBob'),(4,'Postman'),(5,'AppTesting'),(6,'UpdateTest');
/*!40000 ALTER TABLE `Submitters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Chalices`
--

DROP TABLE IF EXISTS `Chalices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Chalices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `areaId` int(11) DEFAULT NULL,
  `depth` tinyint(4) DEFAULT '1',
  `location` varchar(512) DEFAULT NULL,
  `isRoot` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fkChaliceAreaId_idx` (`areaId`),
  CONSTRAINT `fkChaliceAreaId` FOREIGN KEY (`areaId`) REFERENCES `Areas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Chalices`
--

LOCK TABLES `Chalices` WRITE;
/*!40000 ALTER TABLE `Chalices` DISABLE KEYS */;
INSERT INTO `Chalices` VALUES (1,'Pthumeru Chalice',1,1,'Kill Blood-Starved Beast in Old Yharnam',0),(2,'Pthumeru Root Chalice',1,1,'Kill Merciless Watcher in Pthumeru Chalice (layer 2)',1),(3,'Central Pthumeru Chalice',1,2,'Kill Watchdog of the Old Lords in Pthumeru Chalice (layer 3)',0),(4,'Central Pthumeru Root Chalice',1,2,'Kill Keeper of the Old Lords in Central Pthumeru Chalice (layer 2)',1),(5,'Lower Pthumeru Chalice',1,3,'Kill Pthumerian Descendant in Central Pthumeru Chalice (layer 3)',0),(6,'Lower Pthumeru Root Chalice',1,3,'Kill Rom in Lower Pthumeru Chalice (layer 3)',1),(7,'Sinister Lower Pthumeru Root Chalice',1,3,'9000 Blood Echoes, Messenger Shop, Lower Pthumeru Chalice (layer 4)',1),(8,'Defiled Chalice',1,4,'Kill Bloodletting Beast in Lower Pthumeru Chalice (layer 4)',0),(9,'Cursed and Defiled Root Chalice',1,4,'Kill Watchdog of the Old Lords in Defiled Chalice (layer 2)',1),(10,'Great Pthumeru Ihyll Chalice',1,5,'Kill Amygdala in Defiled Chalice (layer 3)',0),(11,'Pthumeru Ihyll Root Chalice',1,5,'Kill Headless Bloodletting Beast in Great Pthumeru Ihyll Chalice (layer 2)',1),(12,'Sinister Pthumeru Ihyll Root Chalice',1,5,'16000 Blood Echoes, Messenger Shop, Great Pthumeru Ihyll Chalice (layer 3 side area)',1),(13,'Hintertomb Chalice',2,2,'In a chest in Central Pthumeru Chalice (layer 2)',0),(14,'Hintertomb Root Chalice',2,2,'Kill Undead Giant in Hintertomb Chalice (layer 2)',1),(15,'Lower Hintertomb Chalice',2,3,'Kill Blood-Starved Beast in Hintertomb Chalice (layer 3)',0),(16,'Lower Hintertomb Root Chalice',2,3,'Kill the Forgotten Madman in Lower Hintertomb Chalice (layer 2)',1),(17,'Sinister Hintertomb Root Chalice',2,3,'10000 Blood Echoes, Messenger Shop, Lower Hintertomb Chalice (layer 3)',1),(18,'Ailing Loran Chalice',3,4,'Kill Amygdala in Nightmare Frontier',0),(19,'Ailing Loran Root Chalice',3,4,'Kill Blood-Starved Beast in Ailing Loran Chalice (layer 2)',1),(20,'Lower Loran Chalice',3,5,'Kill Abhorrent Beast in Ailing Loran Chalice (layer 3)',0),(21,'Lower Ailing Loran Root Chalice',3,5,'Kill Darkbeast Paarl in Lower Loran Chalice (layer 3)',1),(22,'Sinister Lower Loran Root Chalice',3,5,'18000 Blood Echoes, Messenger Shop, Lower Loran Chalice (layer 3)',1),(23,'Great Isz Chalice',4,5,'Kill Ebrietas in Upper Cathedral Ward',0),(24,'Isz Root Chalice',4,5,'Kill Ebrietas in Great Isz Chalice (layer 3)',1),(25,'Sinister Isz Root Chalice',4,5,'20000 Blood Echoes, Messenger Shop, Great Isz Chalice (layer 3)',1);
/*!40000 ALTER TABLE `Chalices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Areas`
--

DROP TABLE IF EXISTS `Areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Areas`
--

LOCK TABLES `Areas` WRITE;
/*!40000 ALTER TABLE `Areas` DISABLE KEYS */;
INSERT INTO `Areas` VALUES (1,'Pthumeru'),(2,'Hintertomb'),(3,'Loran'),(4,'Isz'),(5,'(None)');
/*!40000 ALTER TABLE `Areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'tss_chalice'
--
/*!50003 DROP PROCEDURE IF EXISTS `add_chalice_feature_by_glyph` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_chalice_feature_by_glyph`(glyf VARCHAR(16), featName VARCHAR(32), layr TINYINT)
BEGIN
	DECLARE featId INT;
	DECLARE glyfId INT;	

	SELECT id FROM ChaliceGlyphs WHERE glyph = glyf INTO glyfId;

	IF glyfId IS NOT NULL THEN

		SELECT id FROM Features WHERE name = featName INTO featId;
		IF featId IS NULL THEN
			INSERT INTO Features (name, featureTypeId)
			VALUES (featName, 4);
			SET featId = LAST_INSERT_ID();
		END IF;

		INSERT INTO ChaliceGlyphFeatures (glyphId, featureId, layer)
		VALUES (glyfId, featId, layr);

		SELECT 'Chalice Feature added.' AS Message, LAST_INSERT_ID() AS id;

	ELSE
		SELECT 'Chalice glyph or feature not found!' AS Message, -1 AS id;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_chalice_glyph` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_chalice_glyph`(cid INT, glyphStr VARCHAR(16), isFetid TINYINT(1), isRotted TINYINT(1), isCursed TINYINT(1), isSinister TINYINT(1), submittedBy VARCHAR(64), noteText VARCHAR(512), editPass VARCHAR(64))
BEGIN
	DECLARE submId INT;
	DECLARE checkId INT;

	SELECT id FROM ChaliceGlyphs WHERE chaliceId = cid AND glyph = glyphStr 
		AND fetidOffering = isFetid AND rottedOffering = isRotted AND cursedOffering = isCursed 
		AND sinisterBellOffering = isSinister INTO checkId;

	IF checkId IS NOT NULL THEN
		
		-- we already have it, so error:
		SELECT 'Glyph already found!' AS Message, checkId AS id;

	ELSE
		-- see if we have this submitter
		IF EXISTS(SELECT id FROM Submitters WHERE name = submittedBy) THEN
			SELECT id INTO submId FROM Submitters WHERE name = submittedBy;
		ELSE
			INSERT INTO Submitters (name) VALUES (submittedBy);
			SET submId = LAST_INSERT_ID();
		END IF;

		INSERT INTO ChaliceGlyphs (chaliceId, glyph, fetidOffering, rottedOffering, cursedOffering, sinisterBellOffering, submitterId, workingVotes, nonWorkingVotes, notes, editPassword)
		VALUES (cid, glyphStr, isFetid, isRotted, isCursed, isSinister, submId, 1, 0, noteText, PASSWORD(editPass));

		SELECT 'Glyph added!' AS Message, LAST_INSERT_ID() AS id;

	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_glyph_boss` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_glyph_boss`(glyfId INT, bId INT, layr TINYINT, bossNotes TEXT, editPass VARCHAR(64))
BEGIN
	DECLARE pass VARCHAR(42);
	DECLARE existingBoss INT;
	IF EXISTS(SELECT id FROM ChaliceGlyphs WHERE id = glyfId) THEN
		-- check the password
		SELECT editPassword FROM ChaliceGlyphs WHERE id = glyfId INTO pass;
		IF(pass = PASSWORD(editPass)) THEN
			SELECT id FROM ChaliceGlyphBosses WHERE glyphId = glyfId AND layer = layr INTO existingBoss;
			IF (existingBoss IS NOT NULL) THEN
				UPDATE ChaliceGlyphBosses SET bossId = bId, notes = bossNotes 
				WHERE glyphId = glyfId AND layer = layr;

				SELECT 'Glyph Boss updated.' AS Message, LAST_INSERT_ID() AS id;

			ELSE
				INSERT INTO ChaliceGlyphBosses (glyphId, bossId, layer, notes)
				VALUES (glyfId, bId, layr, bossNotes);

				SELECT 'Glyph Boss added.' AS Message, LAST_INSERT_ID() AS id;

			END IF;

		ELSE
			SELECT 'Edit password incorrect!' AS Message, -2 AS id;
		END IF;
	ELSE
		SELECT 'Glyph not found!' AS Message, -1 AS id;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_glyph_feature` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_glyph_feature`(glyfId INT, featId INT, layr TINYINT, featNotes TEXT, editPass VARCHAR(64))
BEGIN
	DECLARE pass VARCHAR(42);
	IF EXISTS(SELECT id FROM ChaliceGlyphs WHERE id = glyfId) THEN
		-- check the password
		SELECT editPassword FROM ChaliceGlyphs WHERE id = glyfId INTO pass;
		IF(pass = PASSWORD(editPass)) THEN
			INSERT INTO ChaliceGlyphFeatures (glyphId, featureId, layer, notes)
			VALUES (glyfId, featId, layr, featNotes);

			SELECT 'Glyph Feature added.' AS Message, LAST_INSERT_ID() AS id;
		ELSE
			SELECT 'Edit password incorrect!' AS Message, -2 AS id;
		END IF;
	ELSE
		SELECT 'Glyph not found!' AS Message, -1 AS id;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_glyph_vote` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_glyph_vote`(glyphId INT, vote TINYINT(1))
BEGIN
	IF vote = 0 THEN
		UPDATE ChaliceGlyphs SET nonWorkingVotes = nonWorkingVotes + 1 WHERE id = glyphId;
	ELSE
		UPDATE ChaliceGlyphs SET workingVotes = workingVotes + 1 WHERE id = glyphId;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_glyph` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_glyph`(glyfId INT, editPass VARCHAR(64))
BEGIN
	DECLARE pass VARCHAR(42);
	IF EXISTS (SELECT id FROM ChaliceGlyphs WHERE id = glyfId AND editPassword = PASSWORD(editPass)) THEN
		DELETE FROM ChaliceGlyphFeatures WHERE glyphId = glyfId;
		DELETE FROM ChaliceGlyphs WHERE id = glyfId;
		SELECT 'Glyph deleted.' AS Message, 0 AS id;
	ELSE
		SELECT 'Glyph not found or password incorrect!' AS Message, -1 AS id;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_glyph_boss` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_glyph_boss`(bawseId INT, editPass VARCHAR(32))
BEGIN
	DECLARE glyfId INT;
	SELECT glyphId FROM ChaliceGlyphBosses WHERE id = bawseId INTO glyfId;
	IF EXISTS (SELECT id FROM ChaliceGlyphs WHERE id = glyfId AND editPassword = PASSWORD(editPass)) THEN
		DELETE FROM ChaliceGlyphBosses WHERE id = bawseId;
		SELECT 'Boss deleted.' AS Message, 0 AS id;
	ELSE
		SELECT 'Boss not found or password incorrect!' AS Message, -1 AS id;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_glyph_feature` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_glyph_feature`(featId INT, editPass VARCHAR(32))
BEGIN
	DECLARE glyfId INT;
	SELECT glyphId FROM ChaliceGlyphFeatures WHERE id = featId INTO glyfId;
	IF EXISTS (SELECT id FROM ChaliceGlyphs WHERE id = glyfId AND editPassword = PASSWORD(editPass)) THEN
		DELETE FROM ChaliceGlyphFeatures WHERE id = featId;
		SELECT 'Feature deleted.' AS Message, 0 AS id;
	ELSE
		SELECT 'Feature not found or password incorrect!' AS Message, -1 AS id;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `find_chalices_by_materials` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `find_chalices_by_materials`(mat1 INT, q1 INT, mat2 INT, q2 INT, mat3 INT, q3 INT, mat4 INT, q4 INT)
BEGIN
	DECLARE totalRecords INT;
	DECLARE whereClause VARCHAR(256);
	DECLARE joinClause VARCHAR(512);
	DECLARE queryStart VARCHAR(128);
	DECLARE fullQuery VARCHAR(1024);
	DECLARE statement VARCHAR(2048);
	
	SET joinClause = '';

	SET queryStart = 'SELECT t1.chaliceId FROM ChaliceMaterials t1 ';
	-- mat1 will always be provided
	SET whereClause = CONCAT('WHERE (t1.materialId = ', mat1, ' AND t1.quantity <= ', q1, ')');

	IF mat2 IS NOT NULL THEN
		SET joinClause = CONCAT(joinClause, 'INNER JOIN ChaliceMaterials t2 ON t2.chaliceId = t1.chaliceId AND (t2.materialId = ', mat2, ' AND t2.quantity <= ', q2, ') ');
	END IF;
	IF mat3 IS NOT NULL THEN
		SET joinClause = CONCAT(joinClause, 'INNER JOIN ChaliceMaterials t3 ON t3.chaliceId = t1.chaliceId AND (t3.materialId = ', mat3, ' AND t3.quantity <= ', q3, ') ');
	END IF;
	IF mat4 IS NOT NULL THEN
		SET joinClause = CONCAT(joinClause, 'INNER JOIN ChaliceMaterials t4 ON t4.chaliceId = t1.chaliceId AND (t4.materialId = ', mat4, ' AND t4.quantity <= ', q4, ') ');
	END IF;

	SET fullQuery = CONCAT('(', queryStart, joinClause, whereClause, ') ');

	SET @statement = CONCAT('SELECT id, name, location, isRoot FROM Chalices WHERE id IN ', fullQuery);

	-- SELECT statement;
	PREPARE stmt FROM @statement;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

/*
	SELECT COUNT(*) FROM ChaliceGlyphFeatures WHERE featureId = featId INTO totalRecords;

	IF totalRecords > 0 THEN
		SELECT c.name AS ChaliceName, c.depth AS ChaliceDepth, cg.glyph AS Glyph,
		cg.fetidOffering AS isFetid, cg.rottedOffering AS isRotted, cg.cursedOffering AS isCursed, 
		cg.sinisterBellOffering AS isSinister, cg.rating AS GlyphRating, cg.notes AS GlyphNotes,
		f.name AS FeatureName, ft.name as FeatureType, s.name AS Submitter, s.id AS SubmitterId,
		totalRecords AS totalRecords
		FROM ChaliceGlyphs cg
		LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
		LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
		LEFT OUTER JOIN Features f ON f.id = cgf.featureId
		LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
		LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
		WHERE cgf.featureId = featId;
	ELSE
		SELECT 'No glyphs found with that feature!' AS Message, -1 AS Code, 0 as totalRecords;
	END IF;
*/
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `find_glyphs_with_bosses` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `find_glyphs_with_bosses`(boss1 INT, boss2 INT, boss3 INT, boss4 INT)
BEGIN
	DECLARE totalRecords INT;
	DECLARE subQueryWhereClause VARCHAR(256);
	DECLARE joinClause VARCHAR(512);
	DECLARE subQuery VARCHAR(128);
	-- DECLARE fullSubQuery VARCHAR(1024);

	-- DECLARE @inClause VARCHAR(1024);

	DECLARE statement VARCHAR(2048);
	DECLARE orderBy VARCHAR(512);
	-- DECLARE final VARCHAR(4096);
	
	SET @fullSubQuery := '';
	SET @final := '';

	SET joinClause = '';
	SET subQuery = 'SELECT GROUP_CONCAT(cgb1.glyphId SEPARATOR '','') FROM ChaliceGlyphBosses cgb1 ';
	SET subQueryWhereClause = CONCAT(' WHERE cgb1.bossId = ', boss1, ' INTO @inClause');

	IF(boss2 IS NOT NULL) THEN
		SET joinClause = CONCAT(joinClause, 'INNER JOIN ChaliceGlyphBosses cgb2 ON cgb2.bossId = cgb1.glyphId AND cgb2.bossId = ', boss2);
	END IF;

	IF(boss3 IS NOT NULL) THEN
		SET joinClause = CONCAT(joinClause, ' INNER JOIN ChaliceGlyphBosses cgb3 ON cgb3.glyphId = cgb2.glyphId AND cgb3.bossId = ', boss3);
	END IF;

	IF(boss4 IS NOT NULL) THEN
		SET joinClause = CONCAT(joinClause, ' INNER JOIN ChaliceGlyphBosses cgb4 ON cgb4.glyphId = cgb3.glyphId AND cgb4.bossId = ', boss4);
	END IF;

	SET @fullSubQuery := CONCAT(subQuery, joinClause, subQueryWhereClause);

	SET @inClause := '';

	PREPARE stmt FROM @fullSubQuery;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	
	SET @inClause := CONCAT('(', @inClause, ')');

	SET statement = 'SELECT '
		 ' cg.id AS GlyphId'
		', cg.glyph AS Glyph'
		', cg.dateAdded AS DateAdded'
		', c.name AS ChaliceName'
		', c.id AS ChaliceId'
		', cgf.id AS ChaliceFeatureId'
		', cgf.layer AS ChaliceFeatureLayer'
		', cgf.notes AS ChaliceFeatureNotes'
		', f.name AS FeatureName'
		', ft.name AS FeatureTypeName'
		', cg.fetidOffering AS isFetid'
		', cg.rottedOffering AS isRotted'
		', cg.cursedOffering AS isCursed'
		', cg.sinisterBellOffering AS isSinister'
		', s.name AS SubmitterName'
		', cg.workingVotes AS WorkingVotes'
		', cg.nonWorkingVotes AS NonWorkingVotes'
		', cg.notes as Notes '
		'FROM ChaliceGlyphs cg '
		'LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId '
		'LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id '
		'LEFT OUTER JOIN Features f ON f.id = cgf.featureId  '
		'LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId '
		'LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId WHERE cg.id IN ';

	SET orderBy = ' ORDER BY cg.dateAdded DESC';

	IF(@inClause IS NULL) THEN
		SELECT 'No matching glyphs found!' AS message;
	ELSE
		SET @final := CONCAT(statement, @inClause, orderBy);
		-- SELECT @final AS f, @fullSubQuery AS s;
		
		PREPARE finalStmt FROM @final;
		EXECUTE finalStmt;
		DEALLOCATE PREPARE finalStmt;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `find_glyphs_with_features` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `find_glyphs_with_features`(feat1 INT, feat2 INT, feat3 INT, feat4 INT)
BEGIN
	
	DECLARE totalRecords INT;
	DECLARE subQueryWhereClause VARCHAR(256);
	DECLARE joinClause VARCHAR(512);
	DECLARE subQuery VARCHAR(128);
	-- DECLARE fullSubQuery VARCHAR(1024);

	-- DECLARE @inClause VARCHAR(1024);

	DECLARE statement VARCHAR(2048);
	DECLARE orderBy VARCHAR(512);
	-- DECLARE final VARCHAR(4096);
	
	SET @fullSubQuery := '';
	SET @final := '';

	SET joinClause = '';
	SET subQuery = 'SELECT GROUP_CONCAT(cgf1.glyphId SEPARATOR '','') FROM ChaliceGlyphFeatures cgf1 ';
	SET subQueryWhereClause = CONCAT(' WHERE cgf1.featureId = ', feat1, ' INTO @inClause');

	IF(feat2 IS NOT NULL) THEN
		SET joinClause = CONCAT(joinClause, 'INNER JOIN ChaliceGlyphFeatures cgf2 ON cgf2.glyphId = cgf1.glyphId AND cgf2.featureId = ', feat2);
	END IF;

	IF(feat3 IS NOT NULL) THEN
		SET joinClause = CONCAT(joinClause, ' INNER JOIN ChaliceGlyphFeatures cgf3 ON cgf3.glyphId = cgf2.glyphId AND cgf3.featureId = ', feat3);
	END IF;

	IF(feat4 IS NOT NULL) THEN
		SET joinClause = CONCAT(joinClause, ' INNER JOIN ChaliceGlyphFeatures cgf4 ON cgf4.glyphId = cgf3.glyphId AND cgf4.featureId = ', feat4);
	END IF;

	SET @fullSubQuery := CONCAT(subQuery, joinClause, subQueryWhereClause);

	SET @inClause := '';

	PREPARE stmt FROM @fullSubQuery;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	
	SET @inClause := CONCAT('(', @inClause, ')');

	SET statement = 'SELECT '
		 ' cg.id AS GlyphId'
		', cg.glyph AS Glyph'
		', cg.dateAdded AS DateAdded'
		', c.name AS ChaliceName'
		', c.id AS ChaliceId'
		', cgf.id AS ChaliceFeatureId'
		', cgf.layer AS ChaliceFeatureLayer'
		', cgf.notes AS ChaliceFeatureNotes'
		', f.name AS FeatureName'
		', ft.name AS FeatureTypeName'
		', cg.fetidOffering AS isFetid'
		', cg.rottedOffering AS isRotted'
		', cg.cursedOffering AS isCursed'
		', cg.sinisterBellOffering AS isSinister'
		', s.name AS SubmitterName'
		', cg.workingVotes AS WorkingVotes'
		', cg.nonWorkingVotes AS NonWorkingVotes'
		', cg.notes as Notes '
		'FROM ChaliceGlyphs cg '
		'LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId '
		'LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id '
		'LEFT OUTER JOIN Features f ON f.id = cgf.featureId  '
		'LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId '
		'LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId WHERE cg.id IN ';

	SET orderBy = ' ORDER BY cg.dateAdded DESC';

	IF(@inClause IS NULL) THEN
		SELECT 'No matching glyphs found!' AS message;
	ELSE
		SET @final := CONCAT(statement, @inClause, orderBy);
		-- SELECT @final AS f, @fullSubQuery AS s;
		
		PREPARE finalStmt FROM @final;
		EXECUTE finalStmt;
		DEALLOCATE PREPARE finalStmt;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_glyphs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_glyphs`()
BEGIN
	SELECT 
	  cg.id AS GlyphId
	, cg.glyph AS Glyph
	, ch.dateAdded AS DateAdded
	, c.name AS ChaliceName
	, c.id AS ChaliceId
	, cgf.id AS ChaliceFeatureId
	, cgf.layer AS ChaliceFeatureLayer
	, cgf.notes AS ChaliceFeatureNotes
	, f.name AS FeatureName
	, ft.name AS FeatureTypeName
	, cg.fetidOffering AS isFetid
	, cg.rottedOffering AS isRotted
	, cg.cursedOffering AS isCursed
	, cg.sinisterBellOffering AS isSinister
	, s.name AS SubmitterName
	, cg.workingVotes AS WorkingVotes
	, cg.nonWorkingVotes AS NonWorkingVotes
	, cg.notes as Notes
	FROM ChaliceGlyphs cg
	LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
	LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
	LEFT OUTER JOIN Features f ON f.id = cgf.featureId
	LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
	LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
	ORDER BY cg.rating DESC, cg.id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_area_chalices` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_area_chalices`(whichAreaId INT)
BEGIN
	SELECT c.id AS ChaliceId, c.areaId AS AreaId, a.name AS AreaName, c.name AS ChaliceName, c.depth AS Depth, 
	m.id as MaterialId, m.name AS MaterialName, cm.quantity AS Quantity, c.location AS Location,
	c.isRoot AS IsRootChalice, (SELECT COUNT(id) FROM ChaliceGlyphs WHERE chaliceId = c.id) AS glyphCount
	FROM Chalices c
	LEFT OUTER JOIN Areas a ON a.id = c.areaId
	LEFT OUTER JOIN ChaliceMaterials cm ON cm.chaliceId = c.id
	LEFT OUTER JOIN Materials m ON m.id = cm.materialId
	WHERE c.areaId = whichAreaId
	ORDER BY c.depth;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_bosses` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_bosses`(inUse TINYINT(1))
BEGIN
	IF (inUse = 0) THEN
		SELECT id, name FROM Bosses
		ORDER BY name ASC;
	ELSE
		SELECT id, name FROM Bosses
		WHERE id IN(SELECT DISTINCT bossId FROM ChaliceGlyphBosses)
		ORDER BY name ASC;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_chalice` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_chalice`(cid INT)
BEGIN

	DECLARE glyfCount int;
	SELECT COUNT(DISTINCT id) FROM ChaliceGlyphs WHERE chaliceId = cid INTO glyfCount;

	SELECT c.id AS ChaliceId, c.areaId AS AreaId, a.name AS AreaName, c.name AS ChaliceName, c.depth AS Depth, 
	m.id AS MaterialId, m.name AS MaterialName, cm.quantity AS Quantity, c.location AS Location,
	c.isRoot AS IsRootChalice, glyfCount AS glyphCount
	FROM Chalices c
	LEFT OUTER JOIN Areas a ON a.id = c.areaId
	LEFT OUTER JOIN ChaliceMaterials cm ON cm.chaliceId = c.id
	LEFT OUTER JOIN Materials m ON m.id = cm.materialId
	WHERE c.id = cid
	ORDER BY c.areaId, c.depth;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_chalices` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_chalices`(showNonRoot TINYINT)
BEGIN
	IF showNonRoot <> 0 THEN
		SELECT c.id AS ChaliceId, c.areaId AS AreaId, a.name AS AreaName, c.name AS ChaliceName, c.depth AS Depth, 
		m.id AS MaterialId, m.name AS MaterialName, cm.quantity AS Quantity, c.location AS Location,
		c.isRoot AS IsRootChalice, (SELECT COUNT(id) FROM ChaliceGlyphs WHERE chaliceId = c.id) AS glyphCount
		FROM Chalices c
		LEFT OUTER JOIN Areas a ON a.id = c.areaId
		LEFT OUTER JOIN ChaliceMaterials cm ON cm.chaliceId = c.id
		LEFT OUTER JOIN Materials m ON m.id = cm.materialId
		ORDER BY c.areaId, c.depth;
    ELSE
		SELECT c.id AS ChaliceId, c.areaId AS AreaId, a.name AS AreaName, c.name AS ChaliceName, c.depth AS Depth, 
		m.id AS MaterialId, m.name AS MaterialName, cm.quantity AS Quantity, c.location AS Location,
		c.isRoot AS IsRootChalice, (SELECT COUNT(id) FROM ChaliceGlyphs WHERE chaliceId = c.id) AS glyphCount
		FROM Chalices c
		LEFT OUTER JOIN Areas a ON a.id = c.areaId
		LEFT OUTER JOIN ChaliceMaterials cm ON cm.chaliceId = c.id
		LEFT OUTER JOIN Materials m ON m.id = cm.materialId
        WHERE c.isRoot = 1
		ORDER BY c.areaId, c.depth;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_chalices_with_glyphs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_chalices_with_glyphs`()
BEGIN
	SELECT c.id AS ChaliceId, c.areaId AS AreaId, a.name AS AreaName, c.name AS ChaliceName, c.depth AS Depth, 
	m.id as MaterialId, m.name AS MaterialName, cm.quantity AS Quantity, c.location AS Location,
	c.isRoot AS IsRootChalice, (SELECT COUNT(id) FROM ChaliceGlyphs WHERE chaliceId = c.id) AS glyphCount
	FROM Chalices c
	LEFT OUTER JOIN Areas a ON a.id = c.areaId
	LEFT OUTER JOIN ChaliceMaterials cm ON cm.chaliceId = c.id
	LEFT OUTER JOIN Materials m ON m.id = cm.materialId
	WHERE (SELECT COUNT(id) FROM ChaliceGlyphs WHERE chaliceId = c.id) > 0
	ORDER BY c.areaId, c.depth, c.name;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_chalice_offerings` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_chalice_offerings`(cid INT)
BEGIN
	SELECT co.id as ChaliceOfferingId, c.name AS ChaliceName, o.name AS OfferingName, 
	o.description AS OfferingDescription, m.Name AS MaterialName, co.quantity AS Quantity
	FROM ChaliceOfferings co
	LEFT OUTER JOIN Chalices c ON c.id = co.chaliceId
	LEFT OUTER JOIN Offerings o ON o.id = co.offeringId
	LEFT OUTER JOIN Materials m ON m.id = co.materialId
	WHERE co.chaliceId = cid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_features` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_features`()
BEGIN
	SELECT f.id AS FeatureId, f.name AS FeatureName, t.id AS TypeId, t.name AS TypeName
	FROM Features f
	LEFT OUTER JOIN FeatureTypes t ON t.id = f.featureTypeId
	ORDER BY f.featureTypeId, f.name;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_features_by_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_features_by_type`(tId INT)
BEGIN
	SELECT f.id AS FeatureId, f.name AS FeatureName, t.id AS TypeId, t.name AS TypeName
	FROM Features f
	LEFT OUTER JOIN FeatureTypes t ON t.id = f.featureTypeId
	WHERE f.featureTypeId = tId
	ORDER BY f.featureTypeId, f.name;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_glyph` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_glyph`(gId INT)
BEGIN
		SELECT 
		  cg.id AS GlyphId
		, cg.glyph AS Glyph
		, cg.dateAdded AS DateAdded
		, c.name AS ChaliceName
		, c.id AS ChaliceId
		, cgf.id AS ChaliceFeatureId
		, cgf.layer AS ChaliceFeatureLayer
		, cgf.notes AS ChaliceFeatureNotes
		, f.name AS FeatureName
		, ft.name AS FeatureTypeName
		, cg.fetidOffering AS isFetid
		, cg.rottedOffering AS isRotted
		, cg.cursedOffering AS isCursed
		, cg.sinisterBellOffering AS isSinister
		, s.name AS SubmitterName
		, cg.workingVotes AS WorkingVotes
		, cg.nonWorkingVotes AS NonWorkingVotes
		, cg.notes as Notes
		FROM ChaliceGlyphs cg
		LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
		LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
		LEFT OUTER JOIN Features f ON f.id = cgf.featureId
		LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
		LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
		WHERE cg.id = gId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_glyphs_by_chalice` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_glyphs_by_chalice`(cId INT)
BEGIN
	SELECT 
	  cg.id AS GlyphId
	, cg.glyph AS Glyph
	, cg.dateAdded AS DateAdded
	, c.name AS ChaliceName
	, c.id AS ChaliceId
	, cgf.id AS ChaliceFeatureId
	, cgf.layer AS ChaliceFeatureLayer
	, cgf.notes AS ChaliceFeatureNotes
	, f.name AS FeatureName
	, ft.name AS FeatureTypeName
	, cg.fetidOffering AS isFetid
	, cg.rottedOffering AS isRotted
	, cg.cursedOffering AS isCursed
	, cg.sinisterBellOffering AS isSinister
	, s.name AS SubmitterName
	, cg.workingVotes AS WorkingVotes
	, cg.nonWorkingVotes AS NonWorkingVotes
	, cg.notes as Notes
	FROM ChaliceGlyphs cg
	LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
	LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
	LEFT OUTER JOIN Features f ON f.id = cgf.featureId
	LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
	LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
	WHERE cg.chaliceId = cId
	ORDER BY cg.dateAdded DESC, cg.id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_glyphs_by_feature` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_glyphs_by_feature`(featId INT)
BEGIN

	DECLARE totalRecords int;
	SELECT COUNT(*) FROM ChaliceGlyphFeatures WHERE featureId = featId INTO totalRecords;

	IF totalRecords > 0 THEN
		SELECT 
		  cg.id AS GlyphId
		, cg.glyph AS Glyph
		, cg.dateAdded AS DateAdded
		, c.name AS ChaliceName
		, c.id AS ChaliceId
		, cgf.id AS ChaliceFeatureId
		, cgf.layer AS ChaliceFeatureLayer
		, cgf.notes AS ChaliceFeatureNotes
		, f.name AS FeatureName
		, ft.name AS FeatureTypeName
		, cg.fetidOffering AS isFetid
		, cg.rottedOffering AS isRotted
		, cg.cursedOffering AS isCursed
		, cg.sinisterBellOffering AS isSinister
		, s.name AS SubmitterName
		, cg.workingVotes AS WorkingVotes
		, cg.nonWorkingVotes AS NonWorkingVotes
		, cg.notes as Notes
		FROM ChaliceGlyphs cg
		LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
		LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
		LEFT OUTER JOIN Features f ON f.id = cgf.featureId
		LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
		LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
		WHERE featId IN (SELECT featureId FROM ChaliceGlyphFeatures WHERE glyphId = cg.id);
		
	ELSE
		SELECT 'No glyphs found with that feature!' AS Message, -1 AS Code;
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_glyphs_by_submitter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_glyphs_by_submitter`(submitterName VARCHAR(64))
BEGIN

	DECLARE submitterId int;
	SELECT id FROM Submitters WHERE name = submitterName INTO submitterId;

	IF (submitterId IS NOT NULL) THEN
	
		SELECT 
		  cg.id AS GlyphId
		, cg.glyph AS Glyph
		, cg.dateAdded AS DateAdded
		, c.name AS ChaliceName
		, c.id AS ChaliceId
		, cgf.id AS ChaliceFeatureId
		, cgf.layer AS ChaliceFeatureLayer
		, cgf.notes AS ChaliceFeatureNotes
		, f.name AS FeatureName
		, ft.name AS FeatureTypeName
		, cg.fetidOffering AS isFetid
		, cg.rottedOffering AS isRotted
		, cg.cursedOffering AS isCursed
		, cg.sinisterBellOffering AS isSinister
		, s.name AS SubmitterName
		, cg.workingVotes AS WorkingVotes
		, cg.nonWorkingVotes AS NonWorkingVotes
		, cg.notes as Notes
		FROM ChaliceGlyphs cg
		LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
		LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
		LEFT OUTER JOIN Features f ON f.id = cgf.featureId
		LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
		LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
		WHERE cg.submitterId = submitterId
		ORDER BY cg.dateAdded DESC, cg.id;

	ELSE
		SELECT 'Submitter not found!' AS Message, -1 AS Code, 0 as totalRecords;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_glyph_bosses` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_glyph_bosses`(glyfId INT)
BEGIN
	SELECT cgb.id AS id
		, b.name AS name
		, cgb.layer AS layer
		, cgb.notes AS notes
	FROM ChaliceGlyphBosses cgb
	LEFT OUTER JOIN Bosses b ON b.id = cgb.bossId
	WHERE cgb.glyphId = glyfId
	ORDER BY cgb.layer ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_glyph_by_glyph` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_glyph_by_glyph`(glyf VARCHAR(16))
BEGIN
	SELECT 
		  cg.id AS GlyphId
		, cg.glyph AS Glyph
		, cg.dateAdded AS DateAdded
		, c.name AS ChaliceName
		, c.id AS ChaliceId
		, cgf.id AS ChaliceFeatureId
		, cgf.layer AS ChaliceFeatureLayer
		, cgf.notes AS ChaliceFeatureNotes
		, f.name AS FeatureName
		, ft.name AS FeatureTypeName
		, cg.fetidOffering AS isFetid
		, cg.rottedOffering AS isRotted
		, cg.cursedOffering AS isCursed
		, cg.sinisterBellOffering AS isSinister
		, s.name AS SubmitterName
		, cg.workingVotes AS WorkingVotes
		, cg.nonWorkingVotes AS NonWorkingVotes
		, cg.notes as Notes
		FROM ChaliceGlyphs cg
		LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
		LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
		LEFT OUTER JOIN Features f ON f.id = cgf.featureId
		LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
		LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
		WHERE cg.glyph = glyf;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_glyph_features` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_glyph_features`(glyfId INT)
BEGIN
	SELECT cgf.id AS id
		, cgf.layer AS layer
		, cgf.notes AS notes
		, cgf.dateAdded AS dateAdded
		, f.name AS name
	FROM ChaliceGlyphFeatures cgf
	LEFT OUTER JOIN Features f ON f.id = cgf.featureId
	WHERE cgf.glyphId = glyfId
	ORDER BY cgf.dateAdded DESC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_recent_glyphs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_recent_glyphs`()
BEGIN
	SELECT 
	  cg.id AS GlyphId
	, cg.glyph AS Glyph
	, cg.dateAdded AS DateAdded
	, c.name AS ChaliceName
	, c.id AS ChaliceId
	, cgf.id AS ChaliceFeatureId
	, cgf.layer AS ChaliceFeatureLayer
	, cgf.notes AS ChaliceFeatureNotes
	, f.name AS FeatureName
	, ft.name AS FeatureTypeName
	, cg.fetidOffering AS isFetid
	, cg.rottedOffering AS isRotted
	, cg.cursedOffering AS isCursed
	, cg.sinisterBellOffering AS isSinister
	, s.name AS SubmitterName
	, cg.workingVotes AS WorkingVotes
	, cg.nonWorkingVotes AS NonWorkingVotes
	, cg.notes as Notes
	FROM ChaliceGlyphs cg
	LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
	LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
	LEFT OUTER JOIN Features f ON f.id = cgf.featureId
	LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
	LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
	GROUP BY cg.id
	ORDER BY cg.dateAdded DESC, cg.id LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_glyph` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_glyph`(glyfId INT, cId INT, glyf VARCHAR(16), isFetid TINYINT(1), isRotted TINYINT(1), 
								 isCursed TINYINT(1), isSinister TINYINT(1), submittedBy VARCHAR(64), editPass VARCHAR(64))
BEGIN
	DECLARE submId INT;
	DECLARE checkId INT;
	DECLARE pass VARCHAR(42);

	IF EXISTS(SELECT id FROM ChaliceGlyphs WHERE id = glyfId) THEN
		SELECT editPassword FROM ChaliceGlyphs WHERE id = glyfId INTO pass;

		IF (pass = PASSWORD(editPass)) THEN
			
			-- see if we have this submitter
			IF EXISTS(SELECT id FROM Submitters WHERE name = submittedBy) THEN
				SELECT id INTO submId FROM Submitters WHERE name = submittedBy;
			ELSE
				INSERT INTO Submitters (name) VALUES (submittedBy);
				SET submId = LAST_INSERT_ID();
			END IF;

			-- got the submitter, now update the glyph...

			UPDATE ChaliceGlyphs SET chaliceId = cId, glyph = glyf, fetidOffering = isFetid, rottedOffering = isRotted,
			cursedOffering = isCursed, sinisterBellOffering = isSinister, submitterId = submId
			WHERE id = glyfId;

			SELECT 'Glyph updated!' AS Message, glyfId AS id;

		ELSE
			SELECT 'Password incorrect!' AS Message, -1 AS id;


		END IF;

	ELSE
		SELECT 'Glyph not found!' AS Message, -1 AS id;

	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-14 14:06:10
