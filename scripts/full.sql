CREATE DATABASE  IF NOT EXISTS `tss_chalice` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tss_chalice`;
-- MySQL dump 10.13  Distrib 5.6.13, for osx10.6 (i386)
--
-- Host: localhost    Database: tss_chalice
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Areas`
--

DROP TABLE IF EXISTS `Areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Areas`
--

LOCK TABLES `Areas` WRITE;
/*!40000 ALTER TABLE `Areas` DISABLE KEYS */;
INSERT INTO `Areas` VALUES (1,'Pthumeru'),(2,'Hintertomb'),(3,'Loran'),(4,'Isz'),(5,'(None)');
/*!40000 ALTER TABLE `Areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChaliceGlyphFeatures`
--

DROP TABLE IF EXISTS `ChaliceGlyphFeatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChaliceGlyphFeatures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `glyphId` int(11) DEFAULT NULL,
  `featureId` int(11) DEFAULT NULL,
  `layer` tinyint(4) DEFAULT '1',
  `notes` text,
  `dateAdded` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fkCFFeatureId_idx` (`featureId`),
  KEY `fkCGFGlyphId_idx` (`glyphId`),
  CONSTRAINT `fkCGFFeatureId` FOREIGN KEY (`featureId`) REFERENCES `Features` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkCGFGlyphId` FOREIGN KEY (`glyphId`) REFERENCES `ChaliceGlyphs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=439 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChaliceGlyphFeatures`
--

LOCK TABLES `ChaliceGlyphFeatures` WRITE;
/*!40000 ALTER TABLE `ChaliceGlyphFeatures` DISABLE KEYS */;
INSERT INTO `ChaliceGlyphFeatures` VALUES (1,5,1,2,'','2015-06-11 11:03:09'),(2,1,32,2,'','2015-06-11 11:03:09'),(3,1,33,2,'','2015-06-11 11:03:09'),(4,5,1,2,NULL,'2015-06-11 11:03:09'),(5,3,16,1,NULL,'2015-06-11 11:03:09'),(6,3,19,2,NULL,'2015-06-11 11:03:09'),(7,8,7,2,NULL,'2015-06-11 11:03:09'),(8,8,4,3,NULL,'2015-06-11 11:03:09'),(9,9,3,1,NULL,'2015-06-11 11:03:09'),(10,9,3,2,NULL,'2015-06-11 11:03:09'),(11,9,30,3,NULL,'2015-06-11 11:03:09'),(12,10,18,3,NULL,'2015-06-11 11:03:09'),(13,11,27,1,NULL,'2015-06-11 11:03:09'),(14,12,23,1,NULL,'2015-06-11 11:03:09'),(15,12,24,3,NULL,'2015-06-11 11:03:09'),(16,13,17,1,NULL,'2015-06-11 11:03:09'),(17,13,19,3,NULL,'2015-06-11 11:03:09'),(18,14,17,1,NULL,'2015-06-11 11:03:09'),(19,14,19,2,NULL,'2015-06-11 11:03:09'),(20,17,4,1,NULL,'2015-06-11 11:03:09'),(21,17,7,4,NULL,'2015-06-11 11:03:09'),(22,18,19,1,NULL,'2015-06-11 11:03:09'),(23,18,17,3,NULL,'2015-06-11 11:03:09'),(24,19,16,2,NULL,'2015-06-11 11:03:09'),(25,20,28,1,NULL,'2015-06-11 11:03:09'),(26,20,30,2,NULL,'2015-06-11 11:03:09'),(27,22,26,1,NULL,'2015-06-11 11:03:09'),(28,22,1,3,NULL,'2015-06-11 11:03:09'),(29,23,22,1,NULL,'2015-06-11 11:03:09'),(30,24,15,1,NULL,'2015-06-11 11:03:09'),(31,25,10,1,NULL,'2015-06-11 11:03:09'),(32,25,5,3,NULL,'2015-06-11 11:03:09'),(33,26,16,1,NULL,'2015-06-11 11:03:09'),(34,27,7,2,NULL,'2015-06-11 11:03:09'),(35,30,14,2,NULL,'2015-06-11 11:03:09'),(36,32,16,2,NULL,'2015-06-11 11:03:09'),(37,33,10,3,NULL,'2015-06-11 11:03:09'),(38,34,11,3,NULL,'2015-06-11 11:03:09'),(39,34,30,3,NULL,'2015-06-11 11:03:09'),(40,35,27,1,NULL,'2015-06-11 11:03:09'),(41,36,6,2,NULL,'2015-06-11 11:03:09'),(42,36,5,3,NULL,'2015-06-11 11:03:09'),(43,36,10,4,NULL,'2015-06-11 11:03:09'),(44,37,6,1,NULL,'2015-06-11 11:03:09'),(45,37,10,2,NULL,'2015-06-11 11:03:09'),(46,37,9,3,NULL,'2015-06-11 11:03:09'),(47,39,30,1,NULL,'2015-06-11 11:03:09'),(48,40,7,1,NULL,'2015-06-11 11:03:09'),(49,41,8,3,NULL,'2015-06-11 11:03:09'),(50,42,19,1,NULL,'2015-06-11 11:03:09'),(51,43,1,1,NULL,'2015-06-11 11:03:09'),(52,43,26,2,NULL,'2015-06-11 11:03:09'),(53,44,20,1,NULL,'2015-06-11 11:03:09'),(54,44,21,3,NULL,'2015-06-11 11:03:09'),(55,44,15,3,NULL,'2015-06-11 11:03:09'),(56,45,8,1,NULL,'2015-06-11 11:03:09'),(57,45,7,3,NULL,'2015-06-11 11:03:09'),(58,46,16,1,NULL,'2015-06-11 11:03:09'),(59,47,19,1,NULL,'2015-06-11 11:03:09'),(60,47,17,3,NULL,'2015-06-11 11:03:09'),(61,49,11,2,NULL,'2015-06-11 11:03:09'),(62,50,22,2,NULL,'2015-06-11 11:03:09'),(63,50,2,3,NULL,'2015-06-11 11:03:09'),(64,52,17,1,NULL,'2015-06-11 11:03:09'),(65,55,4,3,NULL,'2015-06-11 11:03:09'),(66,57,16,2,NULL,'2015-06-11 11:03:09'),(67,58,28,1,NULL,'2015-06-11 11:03:09'),(68,58,3,3,NULL,'2015-06-11 11:03:09'),(69,59,16,2,NULL,'2015-06-11 11:03:09'),(70,59,19,2,NULL,'2015-06-11 11:03:09'),(71,60,27,1,NULL,'2015-06-11 11:03:09'),(72,62,29,1,NULL,'2015-06-11 11:03:09'),(73,62,24,2,NULL,'2015-06-11 11:03:09'),(74,62,25,2,NULL,'2015-06-11 11:03:09'),(75,63,21,3,NULL,'2015-06-11 11:03:09'),(76,64,22,1,NULL,'2015-06-11 11:03:09'),(77,64,1,3,NULL,'2015-06-11 11:03:09'),(78,65,15,1,NULL,'2015-06-11 11:03:09'),(79,65,13,2,NULL,'2015-06-11 11:03:09'),(80,66,5,2,NULL,'2015-06-11 11:03:09'),(81,66,9,3,NULL,'2015-06-11 11:03:09'),(82,67,3,1,NULL,'2015-06-11 11:03:09'),(83,67,28,2,NULL,'2015-06-11 11:03:09'),(84,67,15,3,NULL,'2015-06-11 11:03:09'),(85,67,11,4,NULL,'2015-06-11 11:03:09'),(86,68,8,1,NULL,'2015-06-11 11:03:09'),(87,69,1,1,NULL,'2015-06-11 11:03:09'),(88,69,26,2,NULL,'2015-06-11 11:03:09'),(89,70,20,1,NULL,'2015-06-11 11:03:09'),(90,71,8,2,NULL,'2015-06-11 11:03:09'),(91,72,2,2,NULL,'2015-06-11 11:03:09'),(92,76,40,2,NULL,'2015-06-11 11:03:09'),(93,82,17,1,NULL,'2015-06-11 11:03:09'),(94,83,4,1,NULL,'2015-06-11 11:03:09'),(95,84,1,1,NULL,'2015-06-11 11:03:09'),(96,85,15,1,NULL,'2015-06-11 11:03:09'),(97,86,1,1,NULL,'2015-06-11 11:03:09'),(98,86,22,3,NULL,'2015-06-11 11:03:09'),(99,88,19,1,NULL,'2015-06-11 11:03:09'),(100,90,29,2,NULL,'2015-06-11 11:03:09'),(101,91,23,1,NULL,'2015-06-11 11:03:09'),(102,91,24,2,NULL,'2015-06-11 11:03:09'),(103,91,25,3,NULL,'2015-06-11 11:03:09'),(104,54,8,2,NULL,'2015-06-11 11:03:09'),(105,93,28,1,NULL,'2015-06-11 11:03:09'),(106,93,3,3,NULL,'2015-06-11 11:03:09'),(107,95,14,2,NULL,'2015-06-11 11:03:09'),(108,97,4,1,NULL,'2015-06-11 11:03:09'),(109,97,7,3,NULL,'2015-06-11 11:03:09'),(110,99,16,2,NULL,'2015-06-11 11:03:09'),(111,100,2,1,NULL,'2015-06-11 11:03:09'),(112,100,22,2,NULL,'2015-06-11 11:03:09'),(113,100,26,3,NULL,'2015-06-11 11:03:09'),(114,100,4,4,NULL,'2015-06-11 11:03:09'),(115,101,28,1,NULL,'2015-06-11 11:03:09'),(116,101,30,2,NULL,'2015-06-11 11:03:09'),(117,101,3,3,NULL,'2015-06-11 11:03:09'),(118,25,10,2,NULL,'2015-06-11 11:03:09'),(119,25,5,3,NULL,'2015-06-11 11:03:09'),(120,103,9,1,NULL,'2015-06-11 11:03:09'),(121,103,14,3,NULL,'2015-06-11 11:03:09'),(122,104,10,2,NULL,'2015-06-11 11:03:09'),(123,105,18,3,NULL,'2015-06-11 11:03:09'),(124,106,14,1,NULL,'2015-06-11 11:03:09'),(125,107,7,1,NULL,'2015-06-11 11:03:09'),(126,108,10,1,NULL,'2015-06-11 11:03:09'),(127,108,5,2,NULL,'2015-06-11 11:03:09'),(128,12,23,1,NULL,'2015-06-11 11:03:09'),(129,12,24,3,NULL,'2015-06-11 11:03:09'),(130,110,1,1,NULL,'2015-06-11 11:03:09'),(131,110,2,3,NULL,'2015-06-11 11:03:09'),(132,111,28,2,NULL,'2015-06-11 11:03:09'),(133,112,21,2,NULL,'2015-06-11 11:03:09'),(134,112,13,3,NULL,'2015-06-11 11:03:09'),(135,113,30,1,NULL,'2015-06-11 11:03:09'),(136,113,28,2,NULL,'2015-06-11 11:03:09'),(137,114,9,2,NULL,'2015-06-11 11:03:09'),(138,114,20,4,NULL,'2015-06-11 11:03:09'),(139,115,7,2,NULL,'2015-06-11 11:03:09'),(140,115,8,3,NULL,'2015-06-11 11:03:09'),(141,116,12,2,NULL,'2015-06-11 11:03:09'),(142,116,13,3,NULL,'2015-06-11 11:03:09'),(143,117,8,2,NULL,'2015-06-11 11:03:09'),(144,117,7,3,NULL,'2015-06-11 11:03:09'),(145,118,29,3,NULL,'2015-06-11 11:03:09'),(146,120,28,2,NULL,'2015-06-11 11:03:09'),(147,120,11,3,NULL,'2015-06-11 11:03:09'),(148,121,27,3,NULL,'2015-06-11 11:03:09'),(149,122,24,1,NULL,'2015-06-11 11:03:09'),(150,123,15,1,NULL,'2015-06-11 11:03:09'),(151,123,13,2,NULL,'2015-06-11 11:03:09'),(152,123,12,3,NULL,'2015-06-11 11:03:09'),(153,124,11,1,NULL,'2015-06-11 11:03:09'),(154,124,30,1,NULL,'2015-06-11 11:03:09'),(155,124,3,2,NULL,'2015-06-11 11:03:09'),(156,124,28,3,NULL,'2015-06-11 11:03:09'),(157,127,8,2,NULL,'2015-06-11 11:03:09'),(158,130,9,4,NULL,'2015-06-11 11:03:09'),(159,132,4,3,NULL,'2015-06-11 11:03:09'),(160,133,6,1,NULL,'2015-06-11 11:03:09'),(161,133,10,2,NULL,'2015-06-11 11:03:09'),(162,133,9,3,NULL,'2015-06-11 11:03:09'),(163,135,6,1,NULL,'2015-06-11 11:03:09'),(164,135,14,3,NULL,'2015-06-11 11:03:09'),(165,136,15,1,NULL,'2015-06-11 11:03:09'),(166,139,20,1,NULL,'2015-06-11 11:03:09'),(167,139,12,1,NULL,'2015-06-11 11:03:09'),(168,141,4,1,NULL,'2015-06-11 11:03:09'),(169,141,7,1,NULL,'2015-06-11 11:03:09'),(170,144,14,3,NULL,'2015-06-11 11:03:09'),(171,146,19,2,NULL,'2015-06-11 11:03:09'),(172,148,9,1,NULL,'2015-06-11 11:03:09'),(173,148,9,3,NULL,'2015-06-11 11:03:09'),(174,149,15,1,NULL,'2015-06-11 11:03:09'),(175,150,14,2,NULL,'2015-06-11 11:03:09'),(176,150,10,3,NULL,'2015-06-11 11:03:09'),(177,151,40,1,NULL,'2015-06-11 11:03:09'),(178,152,9,1,NULL,'2015-06-11 11:03:09'),(179,152,14,3,NULL,'2015-06-11 11:03:09'),(180,153,19,1,NULL,'2015-06-11 11:03:09'),(181,153,17,2,NULL,'2015-06-11 11:03:09'),(182,154,14,2,NULL,'2015-06-11 11:03:09'),(183,154,5,3,NULL,'2015-06-11 11:03:09'),(184,154,9,3,NULL,'2015-06-11 11:03:09'),(185,155,19,1,NULL,'2015-06-11 11:03:09'),(186,155,1,2,NULL,'2015-06-11 11:03:09'),(187,155,3,3,NULL,'2015-06-11 11:03:09'),(188,155,1,4,NULL,'2015-06-11 11:03:09'),(189,156,4,1,NULL,'2015-06-11 11:03:09'),(190,157,4,3,NULL,'2015-06-11 11:03:09'),(191,161,22,1,NULL,'2015-06-11 11:03:09'),(192,162,8,1,NULL,'2015-06-11 11:03:09'),(193,162,4,2,NULL,'2015-06-11 11:03:09'),(194,166,27,2,NULL,'2015-06-11 11:03:09'),(195,167,6,1,NULL,'2015-06-11 11:03:09'),(196,1,32,2,NULL,'2015-06-11 11:03:09'),(197,1,33,2,NULL,'2015-06-11 11:03:09'),(198,6,41,1,NULL,'2015-06-11 11:03:09'),(199,6,41,2,NULL,'2015-06-11 11:03:09'),(200,6,41,3,NULL,'2015-06-11 11:03:09'),(201,6,41,4,NULL,'2015-06-11 11:03:09'),(202,17,42,1,NULL,'2015-06-11 11:03:09'),(203,17,43,1,NULL,'2015-06-11 11:03:09'),(204,17,44,1,NULL,'2015-06-11 11:03:09'),(205,17,45,1,NULL,'2015-06-11 11:03:09'),(206,17,33,1,NULL,'2015-06-11 11:03:09'),(207,17,42,2,NULL,'2015-06-11 11:03:09'),(208,17,43,2,NULL,'2015-06-11 11:03:09'),(209,17,44,2,NULL,'2015-06-11 11:03:09'),(210,17,45,2,NULL,'2015-06-11 11:03:09'),(211,17,33,2,NULL,'2015-06-11 11:03:09'),(212,17,31,2,NULL,'2015-06-11 11:03:09'),(213,17,42,3,NULL,'2015-06-11 11:03:09'),(214,17,43,3,NULL,'2015-06-11 11:03:09'),(215,17,44,3,NULL,'2015-06-11 11:03:09'),(216,17,45,3,NULL,'2015-06-11 11:03:09'),(217,17,33,3,NULL,'2015-06-11 11:03:09'),(218,17,42,4,NULL,'2015-06-11 11:03:09'),(219,17,43,4,NULL,'2015-06-11 11:03:09'),(220,17,44,4,NULL,'2015-06-11 11:03:09'),(221,17,45,4,NULL,'2015-06-11 11:03:09'),(222,17,33,4,NULL,'2015-06-11 11:03:09'),(223,18,41,4,NULL,'2015-06-11 11:03:09'),(224,21,41,1,NULL,'2015-06-11 11:03:09'),(225,21,47,1,NULL,'2015-06-11 11:03:09'),(226,21,48,1,NULL,'2015-06-11 11:03:09'),(227,21,45,1,NULL,'2015-06-11 11:03:09'),(228,22,49,1,NULL,'2015-06-11 11:03:09'),(229,22,42,1,NULL,'2015-06-11 11:03:09'),(230,22,50,1,NULL,'2015-06-11 11:03:09'),(231,22,45,1,NULL,'2015-06-11 11:03:09'),(232,22,49,2,NULL,'2015-06-11 11:03:09'),(233,22,50,2,NULL,'2015-06-11 11:03:09'),(234,22,45,2,NULL,'2015-06-11 11:03:09'),(235,22,50,3,NULL,'2015-06-11 11:03:09'),(236,24,31,1,NULL,'2015-06-11 11:03:09'),(237,25,44,1,NULL,'2015-06-11 11:03:09'),(238,25,33,2,NULL,'2015-06-11 11:03:09'),(239,25,44,3,NULL,'2015-06-11 11:03:09'),(240,27,31,3,NULL,'2015-06-11 11:03:09'),(241,30,43,1,NULL,'2015-06-11 11:03:09'),(242,30,44,1,NULL,'2015-06-11 11:03:09'),(243,30,33,1,NULL,'2015-06-11 11:03:09'),(244,30,43,2,NULL,'2015-06-11 11:03:09'),(245,30,45,2,NULL,'2015-06-11 11:03:09'),(246,30,33,2,NULL,'2015-06-11 11:03:09'),(247,30,45,3,NULL,'2015-06-11 11:03:09'),(248,30,33,3,NULL,'2015-06-11 11:03:09'),(249,41,31,4,NULL,'2015-06-11 11:03:09'),(250,43,50,1,NULL,'2015-06-11 11:03:09'),(251,43,45,2,NULL,'2015-06-11 11:03:09'),(252,43,50,3,NULL,'2015-06-11 11:03:09'),(253,43,45,3,NULL,'2015-06-11 11:03:09'),(254,45,42,2,NULL,'2015-06-11 11:03:09'),(255,45,44,3,NULL,'2015-06-11 11:03:09'),(256,46,41,2,NULL,'2015-06-11 11:03:09'),(257,46,41,3,NULL,'2015-06-11 11:03:09'),(258,49,41,3,NULL,'2015-06-11 11:03:09'),(259,49,48,3,NULL,'2015-06-11 11:03:09'),(260,49,32,3,NULL,'2015-06-11 11:03:09'),(261,49,33,3,NULL,'2015-06-11 11:03:09'),(262,51,33,2,NULL,'2015-06-11 11:03:09'),(263,53,50,2,NULL,'2015-06-11 11:03:09'),(264,53,45,2,NULL,'2015-06-11 11:03:09'),(265,53,45,3,NULL,'2015-06-11 11:03:09'),(266,54,49,1,NULL,'2015-06-11 11:03:09'),(267,54,42,1,NULL,'2015-06-11 11:03:09'),(268,54,42,2,NULL,'2015-06-11 11:03:09'),(269,54,47,3,NULL,'2015-06-11 11:03:09'),(270,54,33,4,NULL,'2015-06-11 11:03:09'),(271,55,31,1,NULL,'2015-06-11 11:03:09'),(272,56,31,3,NULL,'2015-06-11 11:03:09'),(273,59,45,1,NULL,'2015-06-11 11:03:09'),(274,61,45,2,NULL,'2015-06-11 11:03:09'),(275,61,45,3,NULL,'2015-06-11 11:03:09'),(276,63,31,1,NULL,'2015-06-11 11:03:09'),(277,63,32,3,NULL,'2015-06-11 11:03:09'),(278,65,43,1,NULL,'2015-06-11 11:03:09'),(279,65,51,1,NULL,'2015-06-11 11:03:09'),(280,66,43,3,NULL,'2015-06-11 11:03:09'),(281,67,32,4,NULL,'2015-06-11 11:03:09'),(282,69,45,1,NULL,'2015-06-11 11:03:09'),(283,70,51,2,NULL,'2015-06-11 11:03:09'),(284,70,32,3,NULL,'2015-06-11 11:03:09'),(285,70,33,3,NULL,'2015-06-11 11:03:09'),(286,70,51,3,NULL,'2015-06-11 11:03:09'),(287,71,31,2,NULL,'2015-06-11 11:03:09'),(288,71,42,3,NULL,'2015-06-11 11:03:09'),(289,71,44,3,NULL,'2015-06-11 11:03:09'),(290,71,33,3,NULL,'2015-06-11 11:03:09'),(291,71,42,4,NULL,'2015-06-11 11:03:09'),(292,71,44,4,NULL,'2015-06-11 11:03:09'),(293,71,51,4,NULL,'2015-06-11 11:03:09'),(294,72,50,1,NULL,'2015-06-11 11:03:09'),(295,72,45,1,NULL,'2015-06-11 11:03:09'),(296,72,45,2,NULL,'2015-06-11 11:03:09'),(297,74,31,1,NULL,'2015-06-11 11:03:09'),(298,74,42,2,NULL,'2015-06-11 11:03:09'),(299,74,33,2,NULL,'2015-06-11 11:03:09'),(300,74,42,3,NULL,'2015-06-11 11:03:09'),(301,75,47,1,NULL,'2015-06-11 11:03:09'),(302,75,49,1,NULL,'2015-06-11 11:03:09'),(303,76,47,1,NULL,'2015-06-11 11:03:09'),(304,76,45,1,NULL,'2015-06-11 11:03:09'),(305,76,33,1,NULL,'2015-06-11 11:03:09'),(306,76,52,1,NULL,'2015-06-11 11:03:09'),(307,77,31,1,NULL,'2015-06-11 11:03:09'),(308,78,31,2,NULL,'2015-06-11 11:03:09'),(309,80,33,1,NULL,'2015-06-11 11:03:09'),(310,82,41,2,NULL,'2015-06-11 11:03:09'),(311,83,31,1,NULL,'2015-06-11 11:03:09'),(312,83,51,3,NULL,'2015-06-11 11:03:09'),(313,84,45,1,NULL,'2015-06-11 11:03:09'),(314,85,41,1,NULL,'2015-06-11 11:03:09'),(315,85,41,2,NULL,'2015-06-11 11:03:09'),(316,85,41,3,NULL,'2015-06-11 11:03:09'),(317,85,41,4,NULL,'2015-06-11 11:03:09'),(318,54,49,1,NULL,'2015-06-11 11:03:09'),(319,54,42,1,NULL,'2015-06-11 11:03:09'),(320,54,42,2,NULL,'2015-06-11 11:03:09'),(321,54,47,3,NULL,'2015-06-11 11:03:09'),(322,54,42,3,NULL,'2015-06-11 11:03:09'),(323,54,45,3,NULL,'2015-06-11 11:03:09'),(324,54,51,3,NULL,'2015-06-11 11:03:09'),(325,54,33,4,NULL,'2015-06-11 11:03:09'),(326,92,31,2,NULL,'2015-06-11 11:03:09'),(327,92,42,3,NULL,'2015-06-11 11:03:09'),(328,94,43,2,NULL,'2015-06-11 11:03:09'),(329,94,33,3,NULL,'2015-06-11 11:03:09'),(330,25,44,1,NULL,'2015-06-11 11:03:09'),(331,25,33,2,NULL,'2015-06-11 11:03:09'),(332,25,44,3,NULL,'2015-06-11 11:03:09'),(333,102,49,1,NULL,'2015-06-11 11:03:09'),(334,105,52,1,NULL,'2015-06-11 11:03:09'),(335,106,33,3,NULL,'2015-06-11 11:03:09'),(336,106,31,4,NULL,'2015-06-11 11:03:09'),(337,107,42,1,NULL,'2015-06-11 11:03:09'),(338,107,33,1,NULL,'2015-06-11 11:03:09'),(339,107,42,2,NULL,'2015-06-11 11:03:09'),(340,107,33,2,NULL,'2015-06-11 11:03:09'),(341,107,42,3,NULL,'2015-06-11 11:03:09'),(342,108,43,2,NULL,'2015-06-11 11:03:09'),(343,108,44,2,NULL,'2015-06-11 11:03:09'),(344,109,31,1,NULL,'2015-06-11 11:03:09'),(345,112,31,1,NULL,'2015-06-11 11:03:09'),(346,113,32,1,NULL,'2015-06-11 11:03:09'),(347,113,32,2,NULL,'2015-06-11 11:03:09'),(348,113,32,3,NULL,'2015-06-11 11:03:09'),(349,114,32,1,NULL,'2015-06-11 11:03:09'),(350,114,49,2,NULL,'2015-06-11 11:03:09'),(351,114,42,3,NULL,'2015-06-11 11:03:09'),(352,114,43,3,NULL,'2015-06-11 11:03:09'),(353,114,43,4,NULL,'2015-06-11 11:03:09'),(354,114,33,4,NULL,'2015-06-11 11:03:09'),(355,115,42,2,NULL,'2015-06-11 11:03:09'),(356,115,42,3,NULL,'2015-06-11 11:03:09'),(357,115,43,3,NULL,'2015-06-11 11:03:09'),(358,115,44,3,NULL,'2015-06-11 11:03:09'),(359,115,51,3,NULL,'2015-06-11 11:03:09'),(360,115,31,3,NULL,'2015-06-11 11:03:09'),(361,119,31,1,NULL,'2015-06-11 11:03:09'),(362,122,41,1,NULL,'2015-06-11 11:03:09'),(363,122,48,2,NULL,'2015-06-11 11:03:09'),(364,122,43,3,NULL,'2015-06-11 11:03:09'),(365,123,43,3,NULL,'2015-06-11 11:03:09'),(366,124,51,3,NULL,'2015-06-11 11:03:09'),(367,127,42,2,NULL,'2015-06-11 11:03:09'),(368,127,42,3,NULL,'2015-06-11 11:03:09'),(369,127,43,3,NULL,'2015-06-11 11:03:09'),(370,129,31,1,NULL,'2015-06-11 11:03:09'),(371,131,31,1,NULL,'2015-06-11 11:03:09'),(372,131,42,2,NULL,'2015-06-11 11:03:09'),(373,131,33,2,NULL,'2015-06-11 11:03:09'),(374,131,42,3,NULL,'2015-06-11 11:03:09'),(375,131,43,3,NULL,'2015-06-11 11:03:09'),(376,131,44,3,NULL,'2015-06-11 11:03:09'),(377,131,33,3,NULL,'2015-06-11 11:03:09'),(378,137,42,1,NULL,'2015-06-11 11:03:09'),(379,137,49,2,NULL,'2015-06-11 11:03:09'),(380,137,42,3,NULL,'2015-06-11 11:03:09'),(381,137,42,4,NULL,'2015-06-11 11:03:09'),(382,139,51,1,NULL,'2015-06-11 11:03:09'),(383,139,51,2,NULL,'2015-06-11 11:03:09'),(384,139,43,3,NULL,'2015-06-11 11:03:09'),(385,142,41,1,NULL,'2015-06-11 11:03:09'),(386,142,41,2,NULL,'2015-06-11 11:03:09'),(387,142,41,3,NULL,'2015-06-11 11:03:09'),(388,142,41,4,NULL,'2015-06-11 11:03:09'),(389,143,41,1,NULL,'2015-06-11 11:03:09'),(390,143,41,2,NULL,'2015-06-11 11:03:09'),(391,143,41,3,NULL,'2015-06-11 11:03:09'),(392,143,41,4,NULL,'2015-06-11 11:03:09'),(393,144,43,1,NULL,'2015-06-11 11:03:09'),(394,144,50,1,NULL,'2015-06-11 11:03:09'),(395,144,43,2,NULL,'2015-06-11 11:03:09'),(396,144,50,2,NULL,'2015-06-11 11:03:09'),(397,144,33,2,NULL,'2015-06-11 11:03:09'),(398,144,44,3,NULL,'2015-06-11 11:03:09'),(399,144,33,3,NULL,'2015-06-11 11:03:09'),(400,144,31,3,NULL,'2015-06-11 11:03:09'),(401,150,44,2,NULL,'2015-06-11 11:03:09'),(402,150,44,3,NULL,'2015-06-11 11:03:09'),(403,153,41,2,NULL,'2015-06-11 11:03:09'),(404,153,45,3,NULL,'2015-06-11 11:03:09'),(405,154,44,2,NULL,'2015-06-11 11:03:09'),(406,154,44,3,NULL,'2015-06-11 11:03:09'),(407,155,53,1,NULL,'2015-06-11 11:03:09'),(408,155,50,2,NULL,'2015-06-11 11:03:09'),(409,155,42,3,NULL,'2015-06-11 11:03:09'),(410,155,31,4,NULL,'2015-06-11 11:03:09'),(411,156,49,1,NULL,'2015-06-11 11:03:09'),(412,156,42,1,NULL,'2015-06-11 11:03:09'),(413,156,42,2,NULL,'2015-06-11 11:03:09'),(414,156,42,3,NULL,'2015-06-11 11:03:09'),(415,156,33,3,NULL,'2015-06-11 11:03:09'),(416,156,51,3,NULL,'2015-06-11 11:03:09'),(417,156,31,3,NULL,'2015-06-11 11:03:09'),(418,157,42,2,NULL,'2015-06-11 11:03:09'),(419,157,44,2,NULL,'2015-06-11 11:03:09'),(420,157,51,2,NULL,'2015-06-11 11:03:09'),(421,157,42,3,NULL,'2015-06-11 11:03:09'),(422,157,43,3,NULL,'2015-06-11 11:03:09'),(423,157,33,3,NULL,'2015-06-11 11:03:09'),(424,161,45,3,NULL,'2015-06-11 11:03:09'),(425,163,31,3,NULL,'2015-06-11 11:03:09'),(426,164,41,1,NULL,'2015-06-11 11:03:09'),(427,165,41,1,NULL,'2015-06-11 11:03:09'),(428,165,45,1,NULL,'2015-06-11 11:03:09'),(429,165,41,2,NULL,'2015-06-11 11:03:09'),(430,165,45,2,NULL,'2015-06-11 11:03:09'),(431,165,41,3,NULL,'2015-06-11 11:03:09'),(432,165,45,3,NULL,'2015-06-11 11:03:09'),(438,7,75,3,'In side area before lamp','2015-07-17 14:30:26');
/*!40000 ALTER TABLE `ChaliceGlyphFeatures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChaliceGlyphs`
--

DROP TABLE IF EXISTS `ChaliceGlyphs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChaliceGlyphs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chaliceId` int(11) DEFAULT '1',
  `glyph` varchar(16) DEFAULT NULL,
  `fetidOffering` tinyint(1) DEFAULT '0',
  `rottedOffering` tinyint(1) DEFAULT '0',
  `cursedOffering` tinyint(1) DEFAULT '0',
  `sinisterBellOffering` tinyint(1) DEFAULT '0',
  `submitterId` int(11) DEFAULT '1',
  `workingVotes` int(11) DEFAULT '1',
  `nonWorkingVotes` int(11) DEFAULT '0',
  `notes` varchar(512) DEFAULT NULL,
  `editPassword` varchar(42) DEFAULT NULL,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fkCGChaliceId_idx` (`chaliceId`),
  KEY `fkCGSubmitterId_idx` (`submitterId`),
  CONSTRAINT `fkCGChaliceId` FOREIGN KEY (`chaliceId`) REFERENCES `Chalices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkCGSubmitterId` FOREIGN KEY (`submitterId`) REFERENCES `Submitters` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChaliceGlyphs`
--

LOCK TABLES `ChaliceGlyphs` WRITE;
/*!40000 ALTER TABLE `ChaliceGlyphs` DISABLE KEYS */;
INSERT INTO `ChaliceGlyphs` VALUES (1,19,'x98va7xn',0,0,0,0,1,1,1,'Layer 1 boss is Keeper of the Old Lords. Layer 2 materials are all in the bonus area chest room.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(2,2,'v6kuqc2c',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(3,4,'rtddmazf',1,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(5,6,'cnjji6e4',1,1,0,0,1,1,1,'On layer three optional area, there is a Great One Coldblood, this cold blood gives 40,000 echos. Twice as much as the best Kin cold blood.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(6,9,'gh2z72rh',0,0,0,0,1,1,1,'Im looking for other players with high lv 230+ for do this chalice dungeon coop get the chalice for the Queen dungeon and kill her!! Join me!','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(7,11,'nmjapuuu',1,1,0,0,1,1,1,'Ring of Betrothal in side-area before the layer 3 lamp. VERY RARE. ADDITIONAL NOTE: Top Tier Communion rune in Layer 1 Bonus room.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(8,25,'357dsscz',0,0,0,1,1,1,1,'There is two notes in my dungeon about a broken trap that will kill you, and make your souls irretrievable. They are both in the room directly before the Lost Threaded Cane on Layer 3, hug the left wall on the way in, right wall on the way out.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(9,19,'rw4rsf25',1,1,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(10,14,'z79dz9gd',1,0,0,0,1,1,1,'Weapon in l3 predungeon','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(11,16,'gaxrwchu',1,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(12,9,'brkepzqq',1,1,0,0,1,1,1,'Uncanny Reiterpallasch -- Bottom of Spiral Staircase in a treasure room door  Last Level Treasure Room - Lake 5% Physical Dmg Resistance Rune','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(13,4,'xkpxwan2',1,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(14,4,'ggnycrbr',1,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(15,17,'3wsy55du',0,0,0,0,1,1,1,'Great One Coldblood in the subarea right after the first main area! Probably fastest/easiest one to get so far!','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(16,11,'wikifcsa',0,0,0,0,1,1,1,'floor 1 - (side room before boss)Formless Oedon top tier QS bullet max +5 floor 2 - (coffer before main room)+26% blood dmg gem floor 3 - (1st side room coffer)Heir top tier More echoes from Visceral (2nd side room)Clockwise Metamorphasis top tier +15% hp (boss loot)+22%phys dmg gem floor 4 - +23% phys dmg gem and snatcher that drops down poisoned repeatedly and drops twin bloodstones (1-2)','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(17,24,'672kmhsc',1,1,0,0,1,1,1,'I can\'t remember the exact places of the weapons/blood rock since I\'ve been running this dungeon off and on but I know for a fact that they are inside the dungeon, I\'m just taking an educated guess at the locations. And the 4th layer doorway is a Hole/Doorway on the right hand side, it\'s not a door you open just an open hallway.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-06-02 12:32:56'),(18,4,'4yg9ky6b',1,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(19,2,'dcwwhbts',0,0,0,0,1,1,1,'the uncanny saw cleaver is in the bonus section before the first 2nd floor lamp.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(20,19,'pzidw2a7',0,1,0,0,1,1,1,'Layer 3 have no useful materials and items and have Abhorrent Beast boss in end. Don\'t waste your time on layer 3.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(21,6,'cq8txwvz',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(22,6,'2fvj36xy',0,0,0,0,1,1,1,'Uncanny Rifle Spear is in the first loot room, very easy to find. Lost Saw Cleaver is in a rather large optional room before the final boss(Bloodletting Beast)   Lots of Bag men for Ritual Blood (4)','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(23,6,'5hbkfgpn',1,0,0,0,1,1,1,'Hammer is in the side room after the locked door in layer 1','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(24,21,'6krqjavr',1,0,1,0,1,1,1,'Blood rock on the Pre-Main section of layer 1, then Lost Beast claw on the Post-Main section. Bosses are 1) Keeper of the Old Lords. 2) Beast-Possessed Soul. 3) Amygdala.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(25,11,'5wu2zv5n',1,1,0,0,1,1,1,'The second layer has the 15% Health rune. There are only three layers. Some very tricky spots, just run past mobs if you\'re getting stuck. GL!','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(26,2,'cjra37wc',0,0,0,0,1,1,1,'Incredibly easy Uncanny Saw Cleaver pickup. Don\'t bother with the rest as every other layer is filled with death dealer and no good items.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(27,24,'p3umyztu',1,1,1,0,1,1,1,'Eye Gem - Increases chances to get rare item is in the 1st layer','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(28,11,'d5v7f64i',1,1,1,0,1,1,1,'Communion gem - +5vials - 1st Layer Heir Gem - 2nd Layer ? I am not sure if this is 1st or 2nd layer Ring of Betrothal - 3rd layer','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(29,24,'ge5wsydz',0,0,0,0,1,1,1,'good rune first layer','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(30,12,'4y673b6j',0,0,0,1,1,1,1,'There were three runes in this dungeon for me: Formless Oedon, layer 1. Heir, layer 2. Communion, layer 3.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(31,6,'645uxsmh',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(32,2,'pg6anthb',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(33,11,'74cwze3h',1,1,1,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(34,19,'5s5dj7iw',0,0,0,0,1,1,1,'I am not the original creator of this dungeon. I found it searching through the public dungeons.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(35,16,'dt9qe63m',0,0,0,0,1,1,1,'Uncanny Stake Driver is in the optional area aside the boos room.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(36,11,'sz73v83s',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(37,11,'jqcqey6p',0,0,0,0,1,1,1,'There is a room with Double undead Giants in one of the side rooms. There is no reason to fight them.  First boss is Maneating Boar, Second is Triple Watcher, Third is bloodletting beast.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(38,2,'wbg4m9gm',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(39,19,'nf5p55kt',0,0,0,0,1,1,1,'The beast claws are on the first side area of the dungeon in a sarcophagus, fairly easy to find.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(40,25,'byfne74r',0,0,0,1,1,1,1,'Layer 2 and 3 contains runes; 10% arcane dmg reduction and 150 all res. Bosses are Brainsucker, Undead Giant (club) and Ebrietas, so the dungeon is fairly easy in its entirety. ','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(41,24,'rxp5dirg',0,0,0,0,1,1,1,'Can\'t quite remember which floor has what, due to running the dungeon over a couple of days, but it does contain the EYE perfect rune (+100 discovery) and a blood rock on the final layer in an unmarked treasure room. Bosses are Watchers, Brainsucker, Elder Pthumerian, and Amygdala.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(42,4,'2ngjkg5g',0,0,0,0,1,1,1,'Boss 1: Maneater Boar Boss 2: Undead Giant (Club) Boss 3: Pthumerian Descendant','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(43,6,'j7hv5jc6',0,0,0,0,1,1,1,'Great ones cold blood in layer 3','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(44,22,'qm5c4v4p',0,0,0,1,1,1,1,'on layer 2 treasure chest there is dissipating lake rune (+10% bolt dmg reduction)','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(45,24,'v3zfdd7a',1,1,0,0,1,1,1,'Ludwig\'s Blade is after switched door on Layer 1, Deep Sea Rune (+300 Frenzy Resist) on Layer 2 take ladder directly after layer 2 lamp, Kirkhammer is after switched door on Layer 3. Boss1 - Undead Giant; Boss2 - 3x Merciless Executioner; Boss3 - Ebriates','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(46,2,'wq8kgsbp',0,0,0,0,1,1,1,'Uncanny Saw Cleaver can be found in the extra part before the first lantern','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(47,4,'54rhyqgd',1,0,0,0,1,1,1,'Layer 2 has a Depth9 Tempering Blood Gemstone - Phys ATK UP+13.5%','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(48,24,'kjjdqgf9',1,1,0,0,1,1,1,'100 Discovery Eye Rune and 10% Arcane Damage Reduction Arcane Lake runes are within this dungeon. I cannot remember what layers or areas they were in, unfortunately.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(49,19,'avpn9kx5',1,0,0,0,1,1,1,'There\'s a giant bell you can ring in the bonus area at the start of layer 2. I don\'t know if it does anything, but it\'s there. There are a bunch of materials in this but I forgot which ones, I started recording a few of them around layer 3. I know 4 Bastard of Lorans are around layer 1 or 2.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(50,6,'9x4tsmq5',1,1,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(51,11,'5y2p2vzx',0,0,0,0,1,1,1,'25.2% Bloodtinge gem on Layer 2','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(52,4,'7hqg2h9a',0,0,0,0,1,1,1,'layer 2 has a Tempered Blood Gemstone(3) with +13.5% Physical. layer 3 has Ritual Blood(3) in the chest rooms and in the main section of the dungeon. Both are in coffin chests.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(53,6,'868i2ga3',0,0,0,0,1,1,1,'Great One Coldblood on Layer 1 before the main dungeon. LOTS of Sage\'s Wrist in this dungeon if you need them. Formless Oedon (+2 QS Bullets) on Layer 3.  There are also Bath Messengers in a bonus room on Layer 3 (before main dungeon) if you need to resupply.  Recommend just grabbing the Great One Coldblood from the first floor and leaving, though.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(54,24,'u97try9e',0,1,0,0,1,1,1,'On the side-dungeon after layer two\'s boss, the main chest has a Great Deep Sea rune (all RES+150)','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(55,24,'xtbdb2qg',0,0,0,0,1,1,1,'Great Lake rune (5%) on layer 2. Great One Coldblood on layer 4.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(56,11,'53dv69mi',0,0,0,0,1,1,1,'Bosses: Layer 1 - Maneater Pig Layer 2 - Beast-possessed Soul Layer 3 - Bloodletting Beast  Sub Areas on Layer 1 have 2 great runes: Oedon Writhe - V. ATKs grant +3 QS bullets. Clockwise Metamorphosis - +15% max HP','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(57,2,'upppxrnk',0,0,0,0,1,1,1,'One invisible door but leads to room with no treasure :/ Layer 2 weapon is found by exploring first door on left after descending from Layer 1.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(58,19,'9d6vhapi',0,0,0,0,1,1,1,'Fading Lake Rune (+10% Fire DMG Resist) in chest on Layer 1.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(59,4,'57iidjkn',1,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(60,16,'xhkc9uew',0,0,0,0,1,1,1,'The weapon is found on the side path after unlocking the gate to the boss. Go through the side path door and run straight to the only room it\'s a large room but the only threats are the poison pools and the witch who summons scare crows. On the far side of the room where the witch is there should be a ladder. climb it and follow the path to the room with a chest. it\'s guarded by an quick enemy with 2 flaming sickles. The weapon should be in the chest. Enjoy.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(61,4,'gt63us3u',0,0,0,0,1,1,1,'Second boss unlocks Bone Ash set, boasting high fire and bolt defense. Set can be purchased from insight vendor after kill.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(62,9,'aii8vj6a',0,0,0,0,1,1,1,'I forgot exactly what chalice materials are in the dungeon, but there are ritual blood 5 among others like bloodshot eyeball.  first level side room after pulling the lever: Lake Rune +5% physical damage  First boss: Beast Possessed Soul Second boss: 3 Fatties Third Boss: Bloodletting Beast  This dungeon has lots of hemwick witches that summon the scythe dudes and there seems to be at least one in every side dungeon and main (lever) dungeon. The second pre lever dungeon (the first door after the first boss)','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(63,19,'4292egu2',1,1,0,0,1,1,1,'Clawmark rune (Visceral +30%) in layer 2','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(64,6,'45z79m4j',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(65,21,'pzit7wds',0,0,1,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(66,11,'kkzyw7ft',0,0,0,0,1,1,1,'Nothing of note from Level 1. Layer 2 Sub Area before boss contains the Lost Burial Blade. Layer 3 Sub Area before main dungeon contains Lost Reiterpallasch. Layer 3 Sub Area before boss contains the Communion Rune +5 to Blood Vials. Also left with some Sage\'s Hair, plenty of Tomb Mold and some Red Jelly.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(67,19,'bv5ddqt9',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(68,25,'gc2nhw6u',0,0,0,0,1,1,1,'Eye rune (level 3) in the first layer as well. Ludwig\'s blade is in the first room door as soon as you start the dungeon. The Eye rune is once you get into the actual first layer.   First bosses are the Watchmen. Not too bad. Haven\'t explored any other areas yet.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(69,6,'6b7m8sym',0,1,0,0,1,1,1,'Bloodletting Beast is the boss in layer three so be careful, not fully explored so there may be some additional surprises, hence why almost nothing was checked off.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(70,22,'v394hee6',0,0,0,1,1,1,1,'Layer 1: Ritual Blood (5) x2, Uncanny Burial Blade, Boss: Blood-starved Beast  Layer 2: Ritual Blood (5) x8, Bastard of Loran x1, Fading Lake (3), Boss: Undead Giant  Layer 3: Sage\'s Hair x3, Blooming Coldblood Flower x22, Bastard of Loran x1, Anti-Clockwise Metamorphosis (3), Boss: Amygdala','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(71,25,'4utqieu7',0,0,0,0,1,1,1,'Layer 1: Loot: Arcane Lake Rune (3)  Boss: Maneater Boar  Layer 2: Materials: Ritual Blood (5) x5, Yellow Backbone x1, Pearl Slug x2, Sage\'s Hair x2 Loot: Ludwig\'s Lost Holy Blade, Blood Rock Boss: Keeper of the Old Lords  Layer 3:  Materials: Ritual Blood (5) x2, Tomb Mold (5) x14, Pearl Slug x10, Yellow Backbone x3, Bastard of Loran x1 Loot: Kin Coldblood (12) x2, Eye Rune (3), Great Deep Sea Rune (3) Boss: Amygdala','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(72,12,'yif7detn',0,0,0,1,1,1,1,'Chest on layer 3 blocks pathway at one point, just charge attack it to break it and kill that annoying bellmaiden.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(73,4,'y5xqxmy3',1,0,0,0,1,1,1,'Post-area of layer 3 contains a coffin chest with a droplet shaped Tempering Blood Gemstone(3) - physical attack up +13.5% fits any gem slot.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(74,24,'cxdqve9s',1,1,0,0,1,1,1,'Blood rock is in the first door after chamber of the seal. There\'s a ruin for 300 frenzy resist in layer 2, and another ruin for 150 resistance in all categories.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(75,2,'8963az5k',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(76,19,'g3wwmhyh',0,0,0,0,1,1,1,'beast claws in treasure before layer two lamp','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(77,11,'22aa4wfv',0,1,0,0,1,1,1,'Layer 2: Heir 3, side room after the locked door. Layer 3: 15% HP rune, side room before main labyrinth','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(78,12,'9ydgqzy2',0,0,0,1,1,1,1,'Lake rune (7%) on floor 3.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(79,11,'qcwkk72n',0,0,0,0,1,1,1,'It\'s not a root.. I\'m sharing this cuz I need help in boss 2','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(80,9,'aqqcv6vi',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(81,21,'2ufsqve2',0,1,0,0,1,1,1,'Beast Rune +100 Trans is in the first layer in the side room after pulling the lever','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(82,4,'fednzws6',1,0,0,0,1,1,1,'Fairly easy, nice items. Treasure door before second boss contains Tempering blood gemstone (3) droplet type.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(83,24,'ukw8bk83',1,1,1,0,1,1,1,'Layer 2 contains Great Deep Sea rune and Arcane Lake rune.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(84,6,'xamn5v5q',0,0,0,0,1,1,1,'Grat for farming ritual blood, 2 sackguys at the entrances of layer one, another two at the door before the lamp of second floor. Great one coldblood and big amount of items for rituals, among them like 12 tomb(3) mold in a secret room just in frobt of the lamp of the 3rd floor. There was a bunch of things I now dont remember. Easy bosses (watchers, pig and keeper of the old lords. 4th boss is the big dog) also there is a formless oedon Please check it out, you wont regret it','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(85,24,'fmu9qx9g',0,1,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(86,6,'h434qsh7',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(87,9,'kxc8e3p5',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(88,4,'brutm4uf',1,0,0,0,1,1,1,'Low level root with uncanny threaded cane in the first bonus area before layer 1 main area. Good to get your feet wet.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(89,24,'gwsb3sxv',1,1,1,0,1,1,1,'Deep Sea Rune (+300 Frenzy Resist) in Layer 1. Great Deep Sea Rune (+150 All Resist) in Layer 3.  Also found Great One\'s Wisdom in Layer 3 which may or may not be a Blood Rock.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(90,9,'8ushe6kx',1,1,1,0,1,1,1,'Blood Rapture Rune +250 in layer one. LOTS of hunters in this dungeon, and very unfriendly enemy layout in layer 3 with nothing really there. Amygdala is layer 3 boss, so fight only if you feel the need. No good loot in layer 3.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(91,9,'vxtm2uu4',1,1,1,0,1,1,1,'Might\'ve mixed up the order of the layers you get Ludwig\'s and the Reiterpallasch in, but they\'re in the first two layers for certain, with the Chikage in the last layer.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(92,24,'eityvwrj',1,1,1,0,1,1,1,'3 Layers, very easy bosses: Boss 1: Keeper of the Old Lords Boss 2: Undead Giant Boss 3: Amygdala','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(93,19,'8ig6uznt',0,0,0,0,1,1,1,'1 boss Beast Possesed Soul 2 boss Loran Silver Beast 3 boss Abhorrent Beast','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(94,11,'pmiqq34b',1,1,1,0,1,1,1,'Layer 1 has Top Tier Communion Rune (Max Vials +5)  Layer 3 has Top Tier Formless Oedon Rune (Max QS Bullets +5)','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(95,11,'69zmjfm2',1,1,1,0,1,1,1,'- Layer 1 (Bloodletting Beast): Lake Rune [Increases defense. Physical DMG reduction (+7%)] and Heir Rune [More Blood Echoes gained from visceral attacks (+60%)]  - Layer 2 (Beast-possessed Soul):  - Layer 3 (Headless Bloodletting Beast): Great One\'s Wisdom (Could be a Blood Rock or a rune I already had)','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(96,21,'6q77zr9h',0,0,0,0,1,1,1,'Layer 1 (Blood-starved Beast): Clawmark Rune [Strengthens visceral attacks (+30%)]  Layer 2 (Loran Silverbeast): Farmable Hunter (Beast Claw) infront of boss door, commonly drops rank 17 and 18 waning blood gems (I had all three Eye Runes equipped.).  Layer 3 (Abhorrent Beast): Stunning Deep Sea Rune [Increases rapid poison resistance +300]','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(97,24,'9np7c7wk',1,1,1,0,1,1,1,'Brainsucker, Merciless Watchers, Ebrietas, Amygdala','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(98,11,'2t4i6qtv',0,0,0,0,1,1,1,'Chalice dungeon for farming radial cursed gem.  From first lamp go to the dark room, straight up via ladder(careful, 2 enemies are above you, then to the wooden door in the between ledges. By now, you should see the fetid mob you want to farm - he is poisoned, his health bar can be seen through the wall. This guy is both fetid and easily killable and drops 18 lvl cursed gems(maybe 19, yet have to find out).','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(99,2,'d2yxkwhv',0,0,0,0,1,1,1,'bosses are pig, 3 guys, lord watchdogs. Final boss dropped a fire atk +6.3% radial shard.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(100,6,'gd6k45wu',1,1,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(101,19,'h7595mvm',0,0,0,0,1,1,1,'Fading Lake Rune (Fire dmg 7%) in Layer 2. Also there is a loot field in Layer 3 guarded by 2 cannon/axe wielding giants','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(102,11,'k2vmdrct',1,1,0,0,1,1,1,'24.3% Bloodtinge Droplet in the pre dungeon before Layer 2. First boss is Ashen Lord. Layer 2 and beyond have not been completed, venture at your own risk.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(103,11,'rra4n2eq',1,1,0,0,1,1,1,'1st layer found a communion rune for +5 vials. 2nd layer found the ring of betrothal','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(104,11,'4gsfgdnr',1,1,0,0,1,1,1,'Lost Chickage is between the layer 1 boss (Three Watchers) and the next lamp.  After boss 2 (Keeper of the Old Lords) there\'s a Bloodtinge +26% gem in the treasure room.  Third treasure room before Third boss has Tomb Mold 5.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(105,14,'u42p96tx',1,0,0,0,1,1,1,'I put the chalice three times now and I\'ve only get the blood chunk the first time, the other times it was a Madman\'s Knowledge. Is it because of a percentage drop or is it because the chunk just appear one time? Thanks','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(106,11,'byquy9nf',1,1,0,0,1,1,1,'level 1, first sub area - Lake Rune - 7% phys. damage reduction level 1, boss sub area - Lost Logarius Wheel  nothing on level 2 that I could find  level 3, first sub area - Oedon Writhe - +3 QS bullets on visceral attack 7 Sage\'s hair  level 4 has a Blood Rock, 8x Tomb Mold (5) and 12x Ritual Blood (5)','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(107,25,'r82j5jrn',0,0,0,0,1,1,1,'Layer 2 has frenzy 300 resist rune. Layer 3 has arcane damage reduction 7% rune.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(108,11,'v9xwvc7f',1,1,1,0,1,1,1,'Treasure room containing Burial Blade has 3 Red Jellies and 5 Ritual Blood(5)\'s. The area before the burial blade you can find 3 Great One Coldbloods.  Layer 1 boss is the three fatties, Layer 2 boss is Keeper of the Old Lords.  Whatever is in layer 3, I forget, but I recall getting lots of Ritual Blood (5) from that whole layer.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(109,24,'g6hm8zde',0,0,0,0,1,1,1,'I forget what is in this entire dungeon, but the only real thing of note is quite possibly the easiest Blood Rock I\'ve come across. In a side area before the boss door, guarded by a lone Brainsucker.  Amygdala on the final layer seems to be dropping high level Triangle Nourishing gems at a great rate too.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(110,6,'z32f65ah',0,0,0,1,1,1,1,'Didn\'t write down the materials, but there was a Great One Coldblood in the treasure room of Layer 2. Bosses are as follows: Watchers, Beast Possessed Soul, Bloodletting Beast (Normal version).','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(111,22,'2y3kebxd',0,0,0,1,1,1,1,'Anti-Clockwise Metamorphosis +20% stamina on second layer side room','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(112,21,'ees8m5u4',1,1,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(113,19,'v5xiw428',1,1,0,0,1,1,1,'First optional dungeon is a bitch, has a witch and a wolf in a relatively closed in error without a quick escape path.   Bosses so far, beast posessed soul, watched of the old lord','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(114,11,'qukyzqiz',1,1,1,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(115,24,'kamejwyd',1,1,0,0,1,1,1,'Layer 1 Sub area pre dungeon contains an Eye Rune. 3 Watchers Boss.  Layer 2 Sub Area Pre Boss has the Lost Kirkhammer. Brain Sucker Boss.  Layer 3 Sub area pre dungeon contains Lost Ludwigs. Dungeon itself has the bloodrock.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(116,21,'mdisnk76',1,1,1,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(117,24,'8287jfwq',1,1,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(118,9,'3x8kjrh9',0,0,0,0,1,1,1,'Uncanny Wheel was in the bonus area just before the 3rd Layer boss door. It was in a coffin.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(119,11,'jrwffkyp',0,0,0,0,1,1,1,'found blood rock first side area layer 1','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(120,19,'zp7v3rse',1,1,0,0,1,1,1,'Dissipating Lake Rune level 2 (+7% bolt damage reduction) on layer 1.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(121,16,'9p4i7it9',1,0,0,0,1,1,1,'Stunning Deep Sea Rune (+100 rapid poison resistance) found in layer 2, 1st bonus area.  The Uncanny Stake Driver is found in layer 3, 1st bonus area.  This is submitted by /u/MingySpaff.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(122,21,'74ucvg9c',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(123,21,'hfwyuunn',0,1,0,0,1,1,1,'Bosses are Abhorrent Beast, Beast Possessed Soul and Amygdala','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(124,19,'2336ttcm',0,0,0,0,1,1,1,'4 weapons, giant bell, bastards of loran','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(125,19,'akdrub4p',0,0,0,0,1,1,1,'Early Lost Rifle Spear, Uncanny Tonitrus later on. All easy bosses.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(126,11,'k2qdvq7v',1,1,1,0,1,1,1,'L1 quick 3 watcher access good physical gem (radial). L2 hunters with good triangle gems, Formless Oedon, boss beast possessed. L3 Lake, jump under after entry to big room.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(127,24,'7fwgga2i',1,1,1,0,1,1,1,'layer 1 Great Lake 5% and Eye +100 layer 3 Deep Sea +300  bosses maneater boar, brainsucker, pthumerian elder','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(128,9,'cit33pzf',0,0,0,0,1,1,1,'Help with Amy spidere','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(129,25,'8uv4ibay',0,0,0,1,1,1,1,'Great Lake Rune in Layer 2 Layer 1 Boss is Brain Sucker Layer 2 Boss is Watchers','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(130,11,'6qcc6raq',1,0,0,0,1,1,1,'Layer 1 - 15% clockwise metamorph  Layer 2 - Heir rune, tier 3','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(131,24,'rumjp4vc',0,0,0,0,1,1,1,'--Layer 1: Blood rock in door before lamp 1 S.Spear hunter, 1 Wheel hunter -Boss: Keeper of the Old Lords; gave Cursed Sharp Damp triangle gem (6) (17 rating, +SKL scaling) --Layer 2: Deep Sea (+300 frenzy res) in MAIN dungeon area 2 sage\'s hair, 3 pearl slug, 2 ritual blood (5), 2 tomb mold (5) 2 Chikage hunters -Boss: Brainsucker; gave Cursed Tempering Damp triangle gem (6) (18 rating, +% Phys Atk) --Layer 3: Great Deep Sea (+150 all res) in door before lamp 1 red jelly, 4 yellow backbone, 6 sage\'s hair, ','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(132,24,'fnh9c2ui',1,1,1,0,1,1,1,'brother found a lost threaded cane and an eye rune in this dungeon. not sure about the specific layer so my apologies.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(133,11,'as5k238p',1,1,0,0,1,1,1,'4 or 5 Great One Coldbloods in Layer 3','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(134,24,'cg2yf5yf',0,0,0,0,1,1,1,'The first layer before you start the dungeon, in the bonus loot area to the left, at the very end is the +100 Eye Caryll Rune. It\'s in a loot room after a bridge with 4 tricky swinging axes.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(135,12,'zbcde5pw',0,0,0,1,1,1,1,'Blade of Mercy is in First PRE-Area  Logarius Wheel is in Third PRE-Area  Damp bloodtinge gem (6) Rank 19 26.1% Bloodatk on second layer pre-area  Rank 3 Heir Rune Layer 3 main area','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(136,21,'ftrrp55p',1,1,0,0,1,1,1,'Rank 3 Clawmark rune on Layer 3','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(137,2,'csn3g5ja',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(138,11,'4ep9jumw',0,0,0,0,1,1,1,'Layer 1: Beast Possessed Soul. Layer 2: Keeper of The Lords. Layer 3: Blood-letting Beast (no-head version). Layer 3: Pthumerian Elder. Lots of purple haze mobs dropping murky/dirty gems in this 4 layer dungeon. There are a variety of challenging rooms and plenty of NPC hunters to keep things interesting. At this point I have all weapons and runes so I am not sure what is in sarcophagi.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(139,21,'bf589ptf',1,1,1,0,1,1,1,'Uncanny Burial Blade is in the treasure room immediately as you enter the dungeon, before the second lamp. The Lost Stake Driver is in the treasure room before the first boss. You do not have to fight any bosses to obtain either of these. I got a Great One\'s Wisdom from the second or third floor treasure room, so that may be another Lost/Uncanny weapon or a Caryll Rune that I already had.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(140,24,'wxppjy72',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(141,25,'dyjz3xux',0,0,0,0,1,1,1,'Eye rune (level 3) on layer 2','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(142,24,'gragwrhr',0,0,0,0,1,1,1,'just bullshitted to get past the form. Please don\'t waste any time.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(143,2,'6tsghxdg',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(144,11,'2kthkms2',0,0,0,0,1,1,1,'Multiple great one cold bloods and wisdoms','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(145,4,'988sex69',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(146,4,'5us2i2y7',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(147,11,'wc3x5v4n',1,1,0,0,1,1,1,'Layer 1 optional area: rune Clockwise Metamorphosis (Boost max HP+15%) Layer 2 optional area: blood gem Dump Bloodtinge Gem (Blood ATK UP+24.3%) Layer 3 optional area: rune Communion (Max vials held up+5)','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(148,11,'cecqx8mf',1,1,1,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(149,21,'5aa9qchy',1,1,0,0,1,1,1,'Layer 2 : Stunning Deep Sea rune','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(150,12,'ja3i6y6u',0,0,0,1,1,1,1,'Rune Formless Odeon (Max. QS+5) There are 3 illusory walls: first in layer 1 right side of the cannon; second in layer 2 after you jump down in the big room with a chest, in the left side; third is in the layer 3 after the second lamp, on the right side, just opposite of the corridor on the left, and before the boss 3 blue door that must be unlocked. The arena for the third boss it have a big hole in the middle.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(151,19,'w9e89zuu',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(152,11,'bcbgev59',1,1,1,0,1,1,1,'Final Formless Oedon rune in layer 2, i cant remember the exact materials i found but it surely was a lot of tomb mould(5). Have fun!','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(153,4,'jkjvp9da',1,0,0,0,1,1,1,'One treasure chest containing 2 Sage wrist One enemy drops Arcane Haze','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(154,11,'y8tqin2h',1,0,0,0,1,1,1,'I don\'t remember all the materials but there were a few. There are also only 3 layers.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(155,2,'dontknow',0,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(156,24,'454zdmpc',1,1,1,0,1,1,1,'Two runes, fake wall on third layer.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(157,25,'depgdkcp',0,0,0,1,1,1,1,'Sage\'s Hair x6 in the room with Lost Treaded Cane (in optional area Layer 3) Great Lake (5% all dmg reduction) (Layer 1) Arcane Lake (10% Arcane DMG reduction) (Layer 2) Illusory wall under the bridge in Layer 2 Illusory wall in Layer 1 optional area','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(158,22,'4yj72gjz',0,0,0,0,1,1,1,'Lost Beast Claw (I think Layer 1) Lost Stake Driver (I think Layer 2) There is a Bath Messenger in Layer 1','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(159,21,'cdwkecyw',1,1,1,0,1,1,1,'Rune Beast (Transform +100) in Layer 2','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(160,9,'r5i2ztdr',1,1,1,0,1,1,1,'Just started it fresh and looking for others to explore with','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(161,6,'kx5rr6t6',1,1,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(162,24,'jfd25qbz',1,1,1,0,1,1,1,'Layer 1 before lamp- Lost Holy Blade Layer 1 Boss- 3 watcher Layer 2 Before lamp- Lost Cane Layer 2 brain sucker @ switch- Cursed arcane gem (arcane 9.1% and 67.5) Layer 2 Boss- club giant Layer 3 Boss- Emissary, Cursed Abyssal Arcane Gem','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(163,21,'c578hke2',0,0,0,0,1,1,1,'1st Level: Blood Starved Beast 2nd Level: Keeper of the Old Gods 3rd Level: Darkbeast Loran  Bloodrock is in a coffin in Level 3 post-area, before the boss','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(164,4,'yr3s7sgd',1,0,0,0,1,1,1,'','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(165,6,'wg4qc28d',0,0,0,0,1,1,1,'I need help with Rom, the layer 2 boss','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(166,16,'89jdbmhu',1,1,0,0,1,1,1,'Also found a Great One Coldblood and the Great Lake 3% Caryll Rune...think was layer 2 and 3. Have fun :)','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(167,11,'y63p48t5',0,0,0,0,1,1,1,'Ring of Betrothal on layer 1, before the first boss.','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35'),(168,21,'epm7nzrq',1,1,0,0,1,1,1,'Clawmark 3 (+30%) and Stunning Deep Sea 3 (+300 rapid poison res.) runes after first entering and 2nd boss (pre-main area) bonus sections, respectively','*53FF898EB6D67F7D3E8A399515B3976342CFD269','2015-05-25 17:04:35');
/*!40000 ALTER TABLE `ChaliceGlyphs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChaliceMaterials`
--

DROP TABLE IF EXISTS `ChaliceMaterials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChaliceMaterials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chaliceId` int(11) DEFAULT '1',
  `materialId` int(11) DEFAULT '1',
  `quantity` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fkChaliceId_idx` (`chaliceId`),
  KEY `fkMaterialId_idx` (`materialId`),
  CONSTRAINT `fkChaliceId` FOREIGN KEY (`chaliceId`) REFERENCES `Chalices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkMaterialId` FOREIGN KEY (`materialId`) REFERENCES `Materials` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChaliceMaterials`
--

LOCK TABLES `ChaliceMaterials` WRITE;
/*!40000 ALTER TABLE `ChaliceMaterials` DISABLE KEYS */;
INSERT INTO `ChaliceMaterials` VALUES (1,1,1,1000),(2,1,2,2),(3,2,1,1000),(4,2,2,2),(5,2,7,3),(6,3,1,1800),(7,3,3,6),(8,4,1,1800),(9,4,3,8),(10,4,8,6),(11,5,1,3200),(12,5,4,9),(13,6,1,3200),(14,6,4,12),(15,6,9,8),(16,7,1,3200),(17,7,4,12),(18,7,9,8),(19,8,1,5500),(20,8,5,9),(21,8,12,2),(22,8,13,22),(23,9,1,5500),(24,9,5,10),(25,9,10,6),(26,10,1,11500),(27,10,6,9),(28,10,14,4),(29,10,13,25),(30,10,15,1),(31,11,1,11500),(32,11,11,15),(33,11,14,2),(34,11,6,15),(35,12,1,11500),(36,12,11,15),(37,12,14,2),(38,12,6,15),(39,13,1,1800),(40,13,3,3),(41,13,16,3),(42,14,1,1800),(43,14,3,8),(44,14,16,3),(45,14,8,10),(46,15,1,3200),(47,15,4,6),(48,15,16,6),(49,16,1,3200),(50,16,4,12),(51,16,16,6),(52,16,9,3),(53,17,1,3200),(54,17,4,12),(55,17,16,6),(56,17,9,3),(57,18,1,5500),(58,18,5,9),(59,18,17,4),(60,19,1,5500),(61,19,5,14),(62,19,18,3),(63,19,10,3),(64,20,1,11500),(65,20,6,9),(66,20,19,4),(67,21,1,11500),(68,21,6,13),(69,21,19,4),(70,21,11,5),(71,21,14,1),(72,22,1,11500),(73,22,6,13),(74,22,19,4),(75,22,11,5),(76,23,1,11500),(77,23,6,9),(78,23,20,3),(79,23,13,25),(80,24,1,11500),(81,24,6,13),(82,24,20,3),(83,24,14,1),(84,24,11,5),(85,25,1,11500),(86,25,6,13),(87,25,20,3),(88,25,11,5),(89,25,14,1);
/*!40000 ALTER TABLE `ChaliceMaterials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChaliceOfferings`
--

DROP TABLE IF EXISTS `ChaliceOfferings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChaliceOfferings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chaliceId` int(11) DEFAULT '1',
  `offeringId` int(11) DEFAULT '1',
  `materialId` int(11) DEFAULT '1',
  `quantity` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fkChaliceId_idx` (`chaliceId`),
  KEY `fkOfferingId_idx` (`offeringId`),
  KEY `fkMaterialId_idx` (`materialId`),
  KEY `fkOfferingsChaliceId_idx` (`chaliceId`),
  KEY `fkOfferingsOfferingId_idx` (`offeringId`),
  KEY `fkOfferingsMaterialId_idx` (`materialId`),
  CONSTRAINT `fkOfferingsChaliceId` FOREIGN KEY (`chaliceId`) REFERENCES `Chalices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkOfferingsMaterialId` FOREIGN KEY (`materialId`) REFERENCES `Materials` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkOfferingsOfferingId` FOREIGN KEY (`offeringId`) REFERENCES `Offerings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChaliceOfferings`
--

LOCK TABLES `ChaliceOfferings` WRITE;
/*!40000 ALTER TABLE `ChaliceOfferings` DISABLE KEYS */;
INSERT INTO `ChaliceOfferings` VALUES (1,4,1,21,2),(2,6,1,21,4),(3,6,2,22,3),(4,7,4,13,19),(5,9,1,23,3),(6,9,2,22,5),(7,9,3,12,3),(8,11,1,23,6),(9,11,2,24,6),(10,11,3,12,3),(11,12,4,13,32),(12,14,1,21,2),(13,16,1,21,4),(14,16,2,22,3),(15,17,4,13,19),(16,19,1,23,3),(17,19,2,22,5),(18,21,1,23,6),(19,21,2,24,6),(20,21,3,12,3),(21,22,4,13,32),(22,25,4,13,32),(23,24,1,23,6),(24,24,2,24,6),(25,24,3,12,3);
/*!40000 ALTER TABLE `ChaliceOfferings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Chalices`
--

DROP TABLE IF EXISTS `Chalices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Chalices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `areaId` int(11) DEFAULT NULL,
  `depth` tinyint(4) DEFAULT '1',
  `location` varchar(512) DEFAULT NULL,
  `isRoot` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fkChaliceAreaId_idx` (`areaId`),
  CONSTRAINT `fkChaliceAreaId` FOREIGN KEY (`areaId`) REFERENCES `Areas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Chalices`
--

LOCK TABLES `Chalices` WRITE;
/*!40000 ALTER TABLE `Chalices` DISABLE KEYS */;
INSERT INTO `Chalices` VALUES (1,'Pthumeru Chalice',1,1,'Kill Blood-Starved Beast in Old Yharnam',0),(2,'Pthumeru Root Chalice',1,1,'Kill Merciless Watcher in Pthumeru Chalice (layer 2)',1),(3,'Central Pthumeru Chalice',1,2,'Kill Watchdog of the Old Lords in Pthumeru Chalice (layer 3)',0),(4,'Central Pthumeru Root Chalice',1,2,'Kill Keeper of the Old Lords in Central Pthumeru Chalice (layer 2)',1),(5,'Lower Pthumeru Chalice',1,3,'Kill Pthumerian Descendant in Central Pthumeru Chalice (layer 3)',0),(6,'Lower Pthumeru Root Chalice',1,3,'Kill Rom in Lower Pthumeru Chalice (layer 3)',1),(7,'Sinister Lower Pthumeru Root Chalice',1,3,'9000 Blood Echoes, Messenger Shop, Lower Pthumeru Chalice (layer 4)',1),(8,'Defiled Chalice',1,4,'Kill Bloodletting Beast in Lower Pthumeru Chalice (layer 4)',0),(9,'Cursed and Defiled Root Chalice',1,4,'Kill Watchdog of the Old Lords in Defiled Chalice (layer 2)',1),(10,'Great Pthumeru Ihyll Chalice',1,5,'Kill Amygdala in Defiled Chalice (layer 3)',0),(11,'Pthumeru Ihyll Root Chalice',1,5,'Kill Headless Bloodletting Beast in Great Pthumeru Ihyll Chalice (layer 2)',1),(12,'Sinister Pthumeru Ihyll Root Chalice',1,5,'16000 Blood Echoes, Messenger Shop, Great Pthumeru Ihyll Chalice (layer 3 side area)',1),(13,'Hintertomb Chalice',2,2,'In a chest in Central Pthumeru Chalice (layer 2)',0),(14,'Hintertomb Root Chalice',2,2,'Kill Undead Giant in Hintertomb Chalice (layer 2)',1),(15,'Lower Hintertomb Chalice',2,3,'Kill Blood-Starved Beast in Hintertomb Chalice (layer 3)',0),(16,'Lower Hintertomb Root Chalice',2,3,'Kill the Forgotten Madman in Lower Hintertomb Chalice (layer 2)',1),(17,'Sinister Hintertomb Root Chalice',2,3,'10000 Blood Echoes, Messenger Shop, Lower Hintertomb Chalice (layer 3)',1),(18,'Ailing Loran Chalice',3,4,'Kill Amygdala in Nightmare Frontier',0),(19,'Ailing Loran Root Chalice',3,4,'Kill Blood-Starved Beast in Ailing Loran Chalice (layer 2)',1),(20,'Lower Loran Chalice',3,5,'Kill Abhorrent Beast in Ailing Loran Chalice (layer 3)',0),(21,'Lower Ailing Loran Root Chalice',3,5,'Kill Darkbeast Paarl in Lower Loran Chalice (layer 3)',1),(22,'Sinister Lower Loran Root Chalice',3,5,'18000 Blood Echoes, Messenger Shop, Lower Loran Chalice (layer 3)',1),(23,'Great Isz Chalice',4,5,'Kill Ebrietas in Upper Cathedral Ward',0),(24,'Isz Root Chalice',4,5,'Kill Ebrietas in Great Isz Chalice (layer 3)',1),(25,'Sinister Isz Root Chalice',4,5,'20000 Blood Echoes, Messenger Shop, Great Isz Chalice (layer 3)',1);
/*!40000 ALTER TABLE `Chalices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FeatureTypes`
--

DROP TABLE IF EXISTS `FeatureTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FeatureTypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FeatureTypes`
--

LOCK TABLES `FeatureTypes` WRITE;
/*!40000 ALTER TABLE `FeatureTypes` DISABLE KEYS */;
INSERT INTO `FeatureTypes` VALUES (1,'Weapon'),(2,'Blood Gem'),(3,'Upgrade Material'),(4,'Ritual Material'),(5,'Caryll Rune'),(6,'Special');
/*!40000 ALTER TABLE `FeatureTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Features`
--

DROP TABLE IF EXISTS `Features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `featureTypeId` int(11) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fkFeaturesTypeId_idx` (`featureTypeId`),
  CONSTRAINT `fkFeaturesTypeId` FOREIGN KEY (`featureTypeId`) REFERENCES `FeatureTypes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Features`
--

LOCK TABLES `Features` WRITE;
/*!40000 ALTER TABLE `Features` DISABLE KEYS */;
INSERT INTO `Features` VALUES (1,1,'Lost Saw Cleaver'),(2,1,'Lost Saw Spear'),(3,1,'Lost Hunter Axe'),(4,1,'Lost Threaded Cane'),(5,1,'Lost Burial Blade'),(6,1,'Lost Blades of Mercy'),(7,1,'Lost Kirkhammer'),(8,1,'Ludwig\'s Lost Holy Blade'),(9,1,'Lost Reiterpallasch'),(10,1,'Lost Chikage'),(11,1,'Lost Rifle Spear'),(12,1,'Lost Stake Driver'),(13,1,'Lost Tonitrus'),(14,1,'Logarius\' Lost Wheel'),(15,1,'Lost Beast Claws'),(16,1,'Uncanny Saw Cleaver'),(17,1,'Uncanny Saw Spear'),(18,1,'Uncanny Hunter Axe'),(19,1,'Uncanny Threaded Cane'),(20,1,'Uncanny Burial Blade'),(21,1,'Uncanny Blades of Mercy'),(22,1,'Uncanny Kirkhammer'),(23,1,'Ludwig\'s Uncanny Holy Blade'),(24,1,'Uncanny Reiterpallasch'),(25,1,'Uncanny Chikage'),(26,1,'Uncanny Rifle Spear'),(27,1,'Uncanny Stake Driver'),(28,1,'Uncanny Tonitrus'),(29,1,'Logarius\' Uncanny Wheel'),(30,1,'Uncanny Beast Claws'),(31,3,'Blood Stone Rock'),(32,4,'Blooming Coldblood Flower'),(33,4,'Sage\'s Hair'),(40,1,'Beast Claw'),(41,4,'Arcane Haze'),(42,4,'Pearl Slug'),(43,4,'Red Jelly'),(44,4,'Yellow Backbone'),(45,4,'Sage\'s Wrist'),(47,4,'Coldblood Flowerbud'),(48,4,'Coldblood Flower Bulb'),(49,4,'Bloodshot Eyeball'),(50,4,'Inflicted Organ'),(51,4,'Bastard of Loran'),(52,3,'Blood Stone Chunk'),(53,4,'Living String'),(54,5,'Anticlockwise Metamorphosis'),(55,5,'Arcane Lake'),(56,5,'Beast'),(57,5,'Blood Rapture'),(58,5,'Clawmark'),(59,5,'Clear Deep Sea'),(60,5,'Clockwise Metamorphosis'),(61,5,'Communion'),(62,5,'Deep Sea'),(63,5,'Dissipating Lake'),(64,5,'Eye'),(65,5,'Fading Lake'),(66,5,'Formless Odeon'),(67,5,'Great Deep Sea'),(68,5,'Great Lake'),(69,5,'Heir'),(70,5,'Lake'),(71,5,'Moon'),(72,5,'Oedon Writhe'),(73,5,'Stunning Deep Sea'),(75,6,'Ring of Betrothal');
/*!40000 ALTER TABLE `Features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Materials`
--

DROP TABLE IF EXISTS `Materials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Materials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Materials`
--

LOCK TABLES `Materials` WRITE;
/*!40000 ALTER TABLE `Materials` DISABLE KEYS */;
INSERT INTO `Materials` VALUES (1,'Blood Echoes'),(2,'Ritual Blood (1)'),(3,'Ritual Blood (2)'),(4,'Ritual Blood (3)'),(5,'Ritual Blood (4)'),(6,'Ritual Blood (5)'),(7,'Tomb Mold (1)'),(8,'Tomb Mold (2)'),(9,'Tomb Mold (3)'),(10,'Tomb Mold (4)'),(11,'Tomb Mold (5)'),(12,'Bastard of Loran'),(13,'Arcane Haze'),(14,'Red Jelly'),(15,'Living String'),(16,'Bloodshot Eyeball'),(17,'Coldblood Flowerbud'),(18,'Coldblood Flower Bulb'),(19,'Blooming Coldblood Flower'),(20,'Pearl Slug'),(21,'Sage\'s Wrist'),(22,'Inflicted Organ'),(23,'Sage\'s Hair'),(24,'Yellow Backbone');
/*!40000 ALTER TABLE `Materials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Offerings`
--

DROP TABLE IF EXISTS `Offerings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Offerings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Offerings`
--

LOCK TABLES `Offerings` WRITE;
/*!40000 ALTER TABLE `Offerings` DISABLE KEYS */;
INSERT INTO `Offerings` VALUES (1,'Fetid','Stronger enemies, more Blood Echoes for killing, and better drops.'),(2,'Rotted','Chance of Eye Collector, Tomb Prospector or Labyrinth Ritekeeper appearing.  Increased chance of powerful Blood Gems.'),(3,'Cursed','HP cut in half.  20% more Blood Echoes for enemies killed.  Higher chance of Rare Item drops.'),(4,'Sinister Bell','Bellringers appear which summon other players for PvP.  NPC Hunters are summoned if playing offline.');
/*!40000 ALTER TABLE `Offerings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Submitters`
--

DROP TABLE IF EXISTS `Submitters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Submitters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Submitters`
--

LOCK TABLES `Submitters` WRITE;
/*!40000 ALTER TABLE `Submitters` DISABLE KEYS */;
INSERT INTO `Submitters` VALUES (1,'Spreadsheet'),(2,'JovialBob'),(4,'Postman'),(5,'AppTesting');
/*!40000 ALTER TABLE `Submitters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'tss_chalice'
--
/*!50003 DROP PROCEDURE IF EXISTS `add_chalice_feature_by_glyph` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `add_chalice_feature_by_glyph`(glyf VARCHAR(16), featName VARCHAR(32), layr TINYINT)
BEGIN
	DECLARE featId INT;
	DECLARE glyfId INT;	

	SELECT id FROM ChaliceGlyphs WHERE glyph = glyf INTO glyfId;

	IF glyfId IS NOT NULL THEN

		SELECT id FROM Features WHERE name = featName INTO featId;
		IF featId IS NULL THEN
			INSERT INTO Features (name, featureTypeId)
			VALUES (featName, 4);
			SET featId = LAST_INSERT_ID();
		END IF;

		INSERT INTO ChaliceGlyphFeatures (glyphId, featureId, layer)
		VALUES (glyfId, featId, layr);

		SELECT 'Chalice Feature added.' AS Message, LAST_INSERT_ID() AS id;

	ELSE
		SELECT 'Chalice glyph or feature not found!' AS Message, -1 AS id;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_chalice_glyph` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `add_chalice_glyph`(cid INT, glyphStr VARCHAR(16), isFetid TINYINT(1), isRotted TINYINT(1), isCursed TINYINT(1), isSinister TINYINT(1), submittedBy VARCHAR(64), noteText VARCHAR(512), editPass VARCHAR(64))
BEGIN
	DECLARE submId INT;

	IF EXISTS 
		(SELECT id FROM ChaliceGlyphs WHERE chaliceId = cid AND glyph = glyphStr 
		AND fetidOffering = isFetid AND rottedOffering = isRotted AND cursedOffering = isCursed 
		AND sinisterBellOffering = isSinister) THEN
		
		-- we already have it, so error:
		SELECT 'Glyph already found!' AS Message, -1 AS id;

	ELSE
		-- see if we have this submitter
		IF EXISTS(SELECT id FROM Submitters WHERE name = submittedBy) THEN
			SELECT id INTO submId FROM Submitters WHERE name = submittedBy;
		ELSE
			INSERT INTO Submitters (name) VALUES (submittedBy);
			SET submId = LAST_INSERT_ID();
		END IF;

		INSERT INTO ChaliceGlyphs (chaliceId, glyph, fetidOffering, rottedOffering, cursedOffering, sinisterBellOffering, submitterId, rating, notes, editPassword)
		VALUES (cid, glyphStr, isFetid, isRotted, isCursed, isSinister, submId, 0, noteText, PASSWORD(editPass));

		SELECT 'Glyph added!' AS Message, LAST_INSERT_ID() AS id;

	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_glyph_feature` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `add_glyph_feature`(glyfId INT, featId INT, layr TINYINT, featNotes TEXT, editPass VARCHAR(64))
BEGIN
	DECLARE pass VARCHAR(42);
	IF EXISTS(SELECT id FROM ChaliceGlyphs WHERE id = glyfId) THEN
		-- check the password
		SELECT editPassword FROM ChaliceGlyphs WHERE id = glyfId INTO pass;
		IF(pass = PASSWORD(editPass)) THEN
			INSERT INTO ChaliceGlyphFeatures (glyphId, featureId, layer, notes)
			VALUES (glyfId, featId, layr, featNotes);

			SELECT 'Glyph Feature added.' AS Message, LAST_INSERT_ID() AS id;
		ELSE
			SELECT 'Edit password incorrect!' AS Message, -2 AS id;
		END IF;
	ELSE
		SELECT 'Glyph not found!' AS Message, -1 AS id;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_glyph_vote` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `add_glyph_vote`(glyphId INT, vote TINYINT(1))
BEGIN
	IF vote = 0 THEN
		UPDATE ChaliceGlyphs SET nonWorkingVotes = nonWorkingVotes + 1 WHERE id = glyphId;
	ELSE
		UPDATE ChaliceGlyphs SET workingVotes = workingVotes + 1 WHERE id = glyphId;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_glyph` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_glyph`(glyfId INT, editPass VARCHAR(64))
BEGIN
	DECLARE pass VARCHAR(42);
	IF EXISTS (SELECT id FROM ChaliceGlyphs WHERE id = glyfId AND editPassword = PASSWORD(editPass)) THEN
		DELETE FROM ChaliceGlyphFeatures WHERE glyphId = glyfId;
		DELETE FROM ChaliceGlyphs WHERE id = glyfId;
		SELECT 'Glyph deleted.' AS Message, 0 AS id;
	ELSE
		SELECT 'Glyph not found or password incorrect!' AS Message, -1 AS id;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_glyph_feature` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_glyph_feature`(featId INT, editPass VARCHAR(32))
BEGIN
	DECLARE glyfId INT;
	SELECT glyphId FROM ChaliceGlyphFeatures WHERE id = featId INTO glyfId;
	IF EXISTS (SELECT id FROM ChaliceGlyphs WHERE id = glyfId AND editPassword = PASSWORD(editPass)) THEN
		DELETE FROM ChaliceGlyphFeatures WHERE id = featId;
		SELECT 'Feature deleted.' AS Message, 0 AS id;
	ELSE
		SELECT 'Feature not found or password incorrect!' AS Message, -1 AS id;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `find_chalices_by_materials` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `find_chalices_by_materials`(mat1 INT, q1 INT, mat2 INT, q2 INT, mat3 INT, q3 INT, mat4 INT, q4 INT)
BEGIN
	DECLARE totalRecords INT;
	DECLARE whereClause VARCHAR(256);
	DECLARE joinClause VARCHAR(512);
	DECLARE queryStart VARCHAR(128);
	DECLARE fullQuery VARCHAR(1024);
	DECLARE statement VARCHAR(2048);
	
	SET joinClause = '';

	SET queryStart = 'SELECT t1.chaliceId FROM ChaliceMaterials t1 ';
	-- mat1 will always be provided
	SET whereClause = CONCAT('WHERE (t1.materialId = ', mat1, ' AND t1.quantity <= ', q1, ')');

	IF mat2 IS NOT NULL THEN
		SET joinClause = CONCAT(joinClause, 'INNER JOIN ChaliceMaterials t2 ON t2.chaliceId = t1.chaliceId AND (t2.materialId = ', mat2, ' AND t2.quantity <= ', q2, ') ');
	END IF;
	IF mat3 IS NOT NULL THEN
		SET joinClause = CONCAT(joinClause, 'INNER JOIN ChaliceMaterials t3 ON t3.chaliceId = t1.chaliceId AND (t3.materialId = ', mat3, ' AND t3.quantity <= ', q3, ') ');
	END IF;
	IF mat4 IS NOT NULL THEN
		SET joinClause = CONCAT(joinClause, 'INNER JOIN ChaliceMaterials t4 ON t4.chaliceId = t1.chaliceId AND (t4.materialId = ', mat4, ' AND t4.quantity <= ', q4, ') ');
	END IF;

	SET fullQuery = CONCAT('(', queryStart, joinClause, whereClause, ') ');

	SET @statement = CONCAT('SELECT id, name, location, isRoot FROM Chalices WHERE id IN ', fullQuery);

	-- SELECT statement;
	PREPARE stmt FROM @statement;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

/*
	SELECT COUNT(*) FROM ChaliceGlyphFeatures WHERE featureId = featId INTO totalRecords;

	IF totalRecords > 0 THEN
		SELECT c.name AS ChaliceName, c.depth AS ChaliceDepth, cg.glyph AS Glyph,
		cg.fetidOffering AS isFetid, cg.rottedOffering AS isRotted, cg.cursedOffering AS isCursed, 
		cg.sinisterBellOffering AS isSinister, cg.rating AS GlyphRating, cg.notes AS GlyphNotes,
		f.name AS FeatureName, ft.name as FeatureType, s.name AS Submitter, s.id AS SubmitterId,
		totalRecords AS totalRecords
		FROM ChaliceGlyphs cg
		LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
		LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
		LEFT OUTER JOIN Features f ON f.id = cgf.featureId
		LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
		LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
		WHERE cgf.featureId = featId;
	ELSE
		SELECT 'No glyphs found with that feature!' AS Message, -1 AS Code, 0 as totalRecords;
	END IF;
*/
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `find_glyphs_with_features` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `find_glyphs_with_features`(feat1 INT, feat2 INT, feat3 INT, feat4 INT)
BEGIN
	
	DECLARE totalRecords INT;
	DECLARE subQueryWhereClause VARCHAR(256);
	DECLARE joinClause VARCHAR(512);
	DECLARE subQuery VARCHAR(128);
	-- DECLARE fullSubQuery VARCHAR(1024);

	-- DECLARE @inClause VARCHAR(1024);

	DECLARE statement VARCHAR(2048);
	DECLARE orderBy VARCHAR(512);
	-- DECLARE final VARCHAR(4096);
	
	SET @fullSubQuery := '';
	SET @final := '';

	SET joinClause = '';
	SET subQuery = 'SELECT GROUP_CONCAT(cgf1.glyphId SEPARATOR '','') FROM ChaliceGlyphFeatures cgf1 ';
	SET subQueryWhereClause = CONCAT(' WHERE cgf1.featureId = ', feat1, ' INTO @inClause');

	IF(feat2 IS NOT NULL) THEN
		SET joinClause = CONCAT(joinClause, 'INNER JOIN ChaliceGlyphFeatures cgf2 ON cgf2.glyphId = cgf1.glyphId AND cgf2.featureId = ', feat2);
	END IF;

	IF(feat3 IS NOT NULL) THEN
		SET joinClause = CONCAT(joinClause, ' INNER JOIN ChaliceGlyphFeatures cgf3 ON cgf3.glyphId = cgf2.glyphId AND cgf3.featureId = ', feat3);
	END IF;

	IF(feat4 IS NOT NULL) THEN
		SET joinClause = CONCAT(joinClause, ' INNER JOIN ChaliceGlyphFeatures cgf4 ON cgf4.glyphId = cgf3.glyphId AND cgf4.featureId = ', feat4);
	END IF;

	SET @fullSubQuery := CONCAT(subQuery, joinClause, subQueryWhereClause);

	SET @inClause := '';

	PREPARE stmt FROM @fullSubQuery;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	
	SET @inClause := CONCAT('(', @inClause, ')');

	SET statement = 'SELECT '
		 ' cg.id AS GlyphId'
		', cg.glyph AS Glyph'
		', cg.dateAdded AS DateAdded'
		', c.name AS ChaliceName'
		', c.id AS ChaliceId'
		', cgf.id AS ChaliceFeatureId'
		', cgf.layer AS ChaliceFeatureLayer'
		', cgf.notes AS ChaliceFeatureNotes'
		', f.name AS FeatureName'
		', ft.name AS FeatureTypeName'
		', cg.fetidOffering AS isFetid'
		', cg.rottedOffering AS isRotted'
		', cg.cursedOffering AS isCursed'
		', cg.sinisterBellOffering AS isSinister'
		', s.name AS SubmitterName'
		', cg.workingVotes AS WorkingVotes'
		', cg.nonWorkingVotes AS NonWorkingVotes'
		', cg.notes as Notes '
		'FROM ChaliceGlyphs cg '
		'LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId '
		'LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id '
		'LEFT OUTER JOIN Features f ON f.id = cgf.featureId  '
		'LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId '
		'LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId WHERE cg.id IN ';

	SET orderBy = ' ORDER BY cg.dateAdded DESC';

	IF(@inClause IS NULL) THEN
		SELECT 'No matching glyphs found!' AS message;
	ELSE
		SET @final := CONCAT(statement, @inClause, orderBy);
		-- SELECT @final AS f, @fullSubQuery AS s;
		
		PREPARE finalStmt FROM @final;
		EXECUTE finalStmt;
		DEALLOCATE PREPARE finalStmt;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_glyphs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_all_glyphs`()
BEGIN
	SELECT 
	  cg.id AS GlyphId
	, cg.glyph AS Glyph
	, ch.dateAdded AS DateAdded
	, c.name AS ChaliceName
	, c.id AS ChaliceId
	, cgf.id AS ChaliceFeatureId
	, cgf.layer AS ChaliceFeatureLayer
	, cgf.notes AS ChaliceFeatureNotes
	, f.name AS FeatureName
	, ft.name AS FeatureTypeName
	, cg.fetidOffering AS isFetid
	, cg.rottedOffering AS isRotted
	, cg.cursedOffering AS isCursed
	, cg.sinisterBellOffering AS isSinister
	, s.name AS SubmitterName
	, cg.workingVotes AS WorkingVotes
	, cg.nonWorkingVotes AS NonWorkingVotes
	, cg.notes as Notes
	FROM ChaliceGlyphs cg
	LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
	LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
	LEFT OUTER JOIN Features f ON f.id = cgf.featureId
	LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
	LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
	ORDER BY cg.rating DESC, cg.id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_area_chalices` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_area_chalices`(whichAreaId INT)
BEGIN
	SELECT c.id AS ChaliceId, c.areaId AS AreaId, a.name AS AreaName, c.name AS ChaliceName, c.depth AS Depth, 
	m.id as MaterialId, m.name AS MaterialName, cm.quantity AS Quantity, c.location AS Location,
	c.isRoot AS IsRootChalice, (SELECT COUNT(id) FROM ChaliceGlyphs WHERE chaliceId = c.id) AS glyphCount
	FROM Chalices c
	LEFT OUTER JOIN Areas a ON a.id = c.areaId
	LEFT OUTER JOIN ChaliceMaterials cm ON cm.chaliceId = c.id
	LEFT OUTER JOIN Materials m ON m.id = cm.materialId
	WHERE c.areaId = whichAreaId
	ORDER BY c.depth;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_chalice` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_chalice`(cid INT)
BEGIN

	DECLARE glyfCount int;
	SELECT COUNT(DISTINCT id) FROM ChaliceGlyphs WHERE chaliceId = cid INTO glyfCount;

	SELECT c.id AS ChaliceId, c.areaId AS AreaId, a.name AS AreaName, c.name AS ChaliceName, c.depth AS Depth, 
	m.id AS MaterialId, m.name AS MaterialName, cm.quantity AS Quantity, c.location AS Location,
	c.isRoot AS IsRootChalice, glyfCount AS glyphCount
	FROM Chalices c
	LEFT OUTER JOIN Areas a ON a.id = c.areaId
	LEFT OUTER JOIN ChaliceMaterials cm ON cm.chaliceId = c.id
	LEFT OUTER JOIN Materials m ON m.id = cm.materialId
	WHERE c.id = cid
	ORDER BY c.areaId, c.depth;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_chalices` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_chalices`(showNonRoot TINYINT)
BEGIN
	IF showNonRoot <> 0 THEN
		SELECT c.id AS ChaliceId, c.areaId AS AreaId, a.name AS AreaName, c.name AS ChaliceName, c.depth AS Depth, 
		m.id AS MaterialId, m.name AS MaterialName, cm.quantity AS Quantity, c.location AS Location,
		c.isRoot AS IsRootChalice, (SELECT COUNT(id) FROM ChaliceGlyphs WHERE chaliceId = c.id) AS glyphCount
		FROM Chalices c
		LEFT OUTER JOIN Areas a ON a.id = c.areaId
		LEFT OUTER JOIN ChaliceMaterials cm ON cm.chaliceId = c.id
		LEFT OUTER JOIN Materials m ON m.id = cm.materialId
		ORDER BY c.areaId, c.depth;
    ELSE
		SELECT c.id AS ChaliceId, c.areaId AS AreaId, a.name AS AreaName, c.name AS ChaliceName, c.depth AS Depth, 
		m.id AS MaterialId, m.name AS MaterialName, cm.quantity AS Quantity, c.location AS Location,
		c.isRoot AS IsRootChalice, (SELECT COUNT(id) FROM ChaliceGlyphs WHERE chaliceId = c.id) AS glyphCount
		FROM Chalices c
		LEFT OUTER JOIN Areas a ON a.id = c.areaId
		LEFT OUTER JOIN ChaliceMaterials cm ON cm.chaliceId = c.id
		LEFT OUTER JOIN Materials m ON m.id = cm.materialId
        WHERE c.isRoot = 1
		ORDER BY c.areaId, c.depth;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_chalices_with_glyphs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_chalices_with_glyphs`()
BEGIN
	SELECT c.id AS ChaliceId, c.areaId AS AreaId, a.name AS AreaName, c.name AS ChaliceName, c.depth AS Depth, 
	m.id as MaterialId, m.name AS MaterialName, cm.quantity AS Quantity, c.location AS Location,
	c.isRoot AS IsRootChalice, (SELECT COUNT(id) FROM ChaliceGlyphs WHERE chaliceId = c.id) AS glyphCount
	FROM Chalices c
	LEFT OUTER JOIN Areas a ON a.id = c.areaId
	LEFT OUTER JOIN ChaliceMaterials cm ON cm.chaliceId = c.id
	LEFT OUTER JOIN Materials m ON m.id = cm.materialId
	WHERE (SELECT COUNT(id) FROM ChaliceGlyphs WHERE chaliceId = c.id) > 0
	ORDER BY c.areaId, c.depth, c.name;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_chalice_offerings` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_chalice_offerings`(cid INT)
BEGIN
	SELECT co.id as ChaliceOfferingId, c.name AS ChaliceName, o.name AS OfferingName, 
	o.description AS OfferingDescription, m.Name AS MaterialName, co.quantity AS Quantity
	FROM ChaliceOfferings co
	LEFT OUTER JOIN Chalices c ON c.id = co.chaliceId
	LEFT OUTER JOIN Offerings o ON o.id = co.offeringId
	LEFT OUTER JOIN Materials m ON m.id = co.materialId
	WHERE co.chaliceId = cid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_features` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_features`()
BEGIN
	SELECT f.id AS FeatureId, f.name AS FeatureName, t.id AS TypeId, t.name AS TypeName
	FROM Features f
	LEFT OUTER JOIN FeatureTypes t ON t.id = f.featureTypeId
	ORDER BY f.featureTypeId, f.name;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_features_by_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_features_by_type`(tId INT)
BEGIN
	SELECT f.id AS FeatureId, f.name AS FeatureName, t.id AS TypeId, t.name AS TypeName
	FROM Features f
	LEFT OUTER JOIN FeatureTypes t ON t.id = f.featureTypeId
	WHERE f.featureTypeId = tId
	ORDER BY f.featureTypeId, f.name;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_glyph` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_glyph`(gId INT)
BEGIN
		SELECT 
		  cg.id AS GlyphId
		, cg.glyph AS Glyph
		, cg.dateAdded AS DateAdded
		, c.name AS ChaliceName
		, c.id AS ChaliceId
		, cgf.id AS ChaliceFeatureId
		, cgf.layer AS ChaliceFeatureLayer
		, cgf.notes AS ChaliceFeatureNotes
		, f.name AS FeatureName
		, ft.name AS FeatureTypeName
		, cg.fetidOffering AS isFetid
		, cg.rottedOffering AS isRotted
		, cg.cursedOffering AS isCursed
		, cg.sinisterBellOffering AS isSinister
		, s.name AS SubmitterName
		, cg.workingVotes AS WorkingVotes
		, cg.nonWorkingVotes AS NonWorkingVotes
		, cg.notes as Notes
		FROM ChaliceGlyphs cg
		LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
		LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
		LEFT OUTER JOIN Features f ON f.id = cgf.featureId
		LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
		LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
		WHERE cg.id = gId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_glyphs_by_chalice` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_glyphs_by_chalice`(cId INT)
BEGIN
	SELECT 
	  cg.id AS GlyphId
	, cg.glyph AS Glyph
	, cg.dateAdded AS DateAdded
	, c.name AS ChaliceName
	, c.id AS ChaliceId
	, cgf.id AS ChaliceFeatureId
	, cgf.layer AS ChaliceFeatureLayer
	, cgf.notes AS ChaliceFeatureNotes
	, f.name AS FeatureName
	, ft.name AS FeatureTypeName
	, cg.fetidOffering AS isFetid
	, cg.rottedOffering AS isRotted
	, cg.cursedOffering AS isCursed
	, cg.sinisterBellOffering AS isSinister
	, s.name AS SubmitterName
	, cg.workingVotes AS WorkingVotes
	, cg.nonWorkingVotes AS NonWorkingVotes
	, cg.notes as Notes
	FROM ChaliceGlyphs cg
	LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
	LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
	LEFT OUTER JOIN Features f ON f.id = cgf.featureId
	LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
	LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
	WHERE cg.chaliceId = cId
	ORDER BY cg.dateAdded DESC, cg.id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_glyphs_by_feature` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_glyphs_by_feature`(featId INT)
BEGIN

	DECLARE totalRecords int;
	SELECT COUNT(*) FROM ChaliceGlyphFeatures WHERE featureId = featId INTO totalRecords;

	IF totalRecords > 0 THEN
		SELECT 
		  cg.id AS GlyphId
		, cg.glyph AS Glyph
		, cg.dateAdded AS DateAdded
		, c.name AS ChaliceName
		, c.id AS ChaliceId
		, cgf.id AS ChaliceFeatureId
		, cgf.layer AS ChaliceFeatureLayer
		, cgf.notes AS ChaliceFeatureNotes
		, f.name AS FeatureName
		, ft.name AS FeatureTypeName
		, cg.fetidOffering AS isFetid
		, cg.rottedOffering AS isRotted
		, cg.cursedOffering AS isCursed
		, cg.sinisterBellOffering AS isSinister
		, s.name AS SubmitterName
		, cg.workingVotes AS WorkingVotes
		, cg.nonWorkingVotes AS NonWorkingVotes
		, cg.notes as Notes
		FROM ChaliceGlyphs cg
		LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
		LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
		LEFT OUTER JOIN Features f ON f.id = cgf.featureId
		LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
		LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
		WHERE featId IN (SELECT featureId FROM ChaliceGlyphFeatures WHERE glyphId = cg.id);
		
	ELSE
		SELECT 'No glyphs found with that feature!' AS Message, -1 AS Code;
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_glyphs_by_submitter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_glyphs_by_submitter`(submitterName VARCHAR(64))
BEGIN

	DECLARE submitterId int;
	SELECT id FROM Submitters WHERE name = submitterName INTO submitterId;

	IF (submitterId IS NOT NULL) THEN
	
		SELECT 
		  cg.id AS GlyphId
		, cg.glyph AS Glyph
		, cg.dateAdded AS DateAdded
		, c.name AS ChaliceName
		, c.id AS ChaliceId
		, cgf.id AS ChaliceFeatureId
		, cgf.layer AS ChaliceFeatureLayer
		, cgf.notes AS ChaliceFeatureNotes
		, f.name AS FeatureName
		, ft.name AS FeatureTypeName
		, cg.fetidOffering AS isFetid
		, cg.rottedOffering AS isRotted
		, cg.cursedOffering AS isCursed
		, cg.sinisterBellOffering AS isSinister
		, s.name AS SubmitterName
		, cg.workingVotes AS WorkingVotes
		, cg.nonWorkingVotes AS NonWorkingVotes
		, cg.notes as Notes
		FROM ChaliceGlyphs cg
		LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
		LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
		LEFT OUTER JOIN Features f ON f.id = cgf.featureId
		LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
		LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
		WHERE cg.submitterId = submitterId
		ORDER BY cg.dateAdded DESC, cg.id;

	ELSE
		SELECT 'Submitter not found!' AS Message, -1 AS Code, 0 as totalRecords;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_glyph_features` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_glyph_features`(glyfId INT)
BEGIN
	SELECT cgf.id AS id
		, cgf.layer AS layer
		, cgf.notes AS notes
		, cgf.dateAdded AS dateAdded
		, f.name AS name
	FROM ChaliceGlyphFeatures cgf
	LEFT OUTER JOIN Features f ON f.id = cgf.featureId
	WHERE cgf.glyphId = glyfId
	ORDER BY cgf.dateAdded DESC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_recent_glyphs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `get_recent_glyphs`()
BEGIN
	SELECT 
	  cg.id AS GlyphId
	, cg.glyph AS Glyph
	, cg.dateAdded AS DateAdded
	, c.name AS ChaliceName
	, c.id AS ChaliceId
	, cgf.id AS ChaliceFeatureId
	, cgf.layer AS ChaliceFeatureLayer
	, cgf.notes AS ChaliceFeatureNotes
	, f.name AS FeatureName
	, ft.name AS FeatureTypeName
	, cg.fetidOffering AS isFetid
	, cg.rottedOffering AS isRotted
	, cg.cursedOffering AS isCursed
	, cg.sinisterBellOffering AS isSinister
	, s.name AS SubmitterName
	, cg.workingVotes AS WorkingVotes
	, cg.nonWorkingVotes AS NonWorkingVotes
	, cg.notes as Notes
	FROM ChaliceGlyphs cg
	LEFT OUTER JOIN Chalices c ON c.id = cg.chaliceId
	LEFT OUTER JOIN ChaliceGlyphFeatures cgf ON cgf.glyphId = cg.id
	LEFT OUTER JOIN Features f ON f.id = cgf.featureId
	LEFT OUTER JOIN FeatureTypes ft ON ft.id = f.featureTypeId
	LEFT OUTER JOIN Submitters s ON s.id = cg.submitterId
	GROUP BY cg.id
	ORDER BY cg.dateAdded DESC, cg.id LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-17 11:05:24
