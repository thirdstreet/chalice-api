/**
 * Created by damonfloyd on 5/5/15.
 */

var datamassager = {
    massage: function(rawData) {
        var rows = rawData[0];
        //console.log('rows:', rows);
        var rowCount = rows.length;
        //console.log('rowCount: ', rowCount, ', length:', rows.length);
        if(rows.length === 1) {
            rows = rows[0];
            //console.log('single row:', rows);
        }
        var data = {rows: rows, rowCount: rowCount};
        return data;
    },

    flattenChalices: function(rawChalices) {
        var chalices = [];
        var seen = {};
        var index;
        for(var i = 0; i < rawChalices[0].length; i++) {
            var c = rawChalices[0][i];
            if(seen.hasOwnProperty(c.ChaliceId)) {
                //console.log('already seen: ', c.ChaliceId);
                var material = {
                    materialId: c.MaterialId,
                    materialName: c.MaterialName,
                    materialQuantity: c.Quantity
                };
                chalices[index].materials.push(material);
            }
            else {
                //console.log('new chalice: ', c.ChaliceId);
                var chalice = {
                    chaliceId: c.ChaliceId,
                    chaliceName: c.ChaliceName,
                    areaId: c.AreaId,
                    areaName: c.AreaName,
                    glyphCount: c.glyphCount,
                    depth: c.Depth,
                    location: c.Location,
                    isRoot: c.isRootChalice,
                    materials: [
                        {
                            materialId: c.MaterialId,
                            materialName: c.MaterialName,
                            materialQuantity: c.Quantity
                        }
                    ]
                };
                chalices.push(chalice);
                seen[c.ChaliceId] = 1;
                index = (chalices.length) - 1;
            }
        }

        return chalices;
    },

    flattenGlyphs: function(rawGlyphs) {
        var i = 0;
        var glyphs = [];
        var seenGlyphs = {};
        var index;
        for(i = 0; i < rawGlyphs[0].length; i++) {
            var g = rawGlyphs[0][i];
            //console.log(g);
            if(!g.hasOwnProperty('message')) {
                if (seenGlyphs.hasOwnProperty(g.GlyphId)) {
                    // already got it.  So we just need the feature
                    var feature = {
                        featureId: g.ChaliceFeatureId,
                        layer: g.ChaliceFeatureLayer,
                        notes: g.ChaliceFeatureNotes,
                        featureName: g.FeatureName,
                        featureType: g.FeatureTypeName
                    };
                    // push feature onto glyph's feature array
                    glyphs[index].features.push(feature);
                }
                else {
                    // don't have it, so new glyph...
                    var glyph = {
                        glyphId: g.GlyphId,
                        glyph: g.Glyph,
                        chaliceName: g.ChaliceName,
                        chaliceId: g.ChaliceId,
                        isFetid: g.isFetid,
                        isRotted: g.isRotted,
                        isCursed: g.isCursed,
                        isSinister: g.isSinister,
                        submitter: g.SubmitterName,
                        rating: g.Rating,
                        notes: g.Notes,
                        dateAdded: g.DateAdded,
                        workingVotes: g.WorkingVotes,
                        nonWorkingVotes: g.NonWorkingVotes,
                        features: [
                            {
                                featureId: g.ChaliceFeatureId,
                                layer: g.ChaliceFeatureLayer,
                                notes: g.ChaliceFeatureNotes,
                                featureName: g.FeatureName,
                                featureType: g.FeatureTypeName
                            }
                        ]
                    };

                    // add it
                    glyphs.push(glyph);
                    seenGlyphs[g.GlyphId] = 1;
                    index = (glyphs.length) - 1;
                }
            }
        }
        return glyphs;
    }
};

module.exports = datamassager;
