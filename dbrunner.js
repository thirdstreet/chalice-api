/**
 * Created by damonfloyd on 4/27/15.
 */
var mysql = require('mysql');
var dbinfo = require('./secret/dbinfo.json');
var pool;

module.exports = {

    create: function () {
        //console.log('creating pool...');
        pool = mysql.createPool({
            connectionLimit: 100,
            host: '127.0.0.1',
            user: dbinfo.user,
            password: dbinfo.password,
            database: dbinfo.database,
            debug: false
        });
        //console.log('pool created...');
        return this;
    },

    format: function (sql, inserts) {
        return mysql.format(sql, inserts);
    },

    run: function (query, data, cb) {
        // make sure we have a callback if the user didn't provide one
        if (typeof data === 'function') {
            cb = data;
        }
        cb = cb || function () {
            };
        //console.log('getting connection to run: ' + query);
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                cb(err, undefined);
            }
            else {
                conn.query(query, data, cb);
                conn.release();
            }
        });
    }
};
