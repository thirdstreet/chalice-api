/**
 * Created by damonfloyd on 7/31/15.
 */
var express = require('express');
var bossRouter = express.Router();
var dbrunner = require('../dbrunner');
var dm = require('../lib/datamassager');

bossRouter.use(function logreq(req, res, next) {
    console.log('Bosses:',req.method, req.url, req.hostname);
    next();
});

// return a list of bosses
bossRouter.get('/', function(req, res) {
    var sql = 'CALL get_bosses(?)';
    var args = [];
    args.push(0);
    dbrunner.run(sql, args, function(err, data) {
        if(err) {
            console.log(err);
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    });
});

// delete a boss from a glyph
bossRouter.delete('/:Id', function(req, res) {
    var id = req.params.Id;
    var editPass = req.body.editPass;
    var sql = 'CALL delete_glyph_boss(?,?)';
    var args = [];
    args.push(id, editPass);
    dbrunner.run(sql, args, function(err, data) {
        if(err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    });
});

//add a new boss (this updates if a boss for that layer already exists)
bossRouter.post('/', function(req, res) {
    var glyphId = req.body.glyphId;
    var bossId = req.body.bossId;
    var layer = req.body.layer;
    var notes = req.body.notes;
    var editPass = req.body.editPass;
    var args = [];
    args.push(glyphId, bossId, layer, notes, editPass);
    var sql = 'CALL add_glyph_boss(?,?,?,?,?)';
    dbrunner.run(sql, args, function(err, data) {
        if(err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    });
});

module.exports = bossRouter;