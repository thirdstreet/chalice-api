/**
 * Created by damonfloyd on 5/28/15.
 */
var express = require('express');
var featureRouter = express.Router();
var dbrunner = require('../dbrunner');
var dm = require('../lib/datamassager');

featureRouter.use(function logreq(req, res, next) {
    console.log('Features:',req.method, req.url, req.hostname);
    next();
});

// return a list of features
featureRouter.get('/', function(req, res) {
    var sql = 'CALL get_features()';
    console.log(sql);
    dbrunner.run(sql, [], function(err, data) {
        if(err) {
	    console.log(err);
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    });
});

featureRouter.get('/type/:Id', function(req, reqres) {
    var sql = 'CALL get_features_by_type(?)';
    dbrunner.run(sql, [req.params.Id], function(err, data) {
        if(err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    });
});

featureRouter.get('/:GlyphId', function(req, res) {
    var sql = 'CALL get_glyph_features(?)';
    dbrunner.run(sql, [req.params.GlyphId], function(err, data) {
        if(err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    });
});

featureRouter.put('/:Id', function(req, res) {
    console.log(req.params.Id, req.body.editPass);
    var sql = 'CALL delete_glyph_feature(?,?)';
    dbrunner.run(sql, [req.params.Id, req.body.editPass], function(err, data) {
        if(err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    });
});

module.exports = featureRouter;
