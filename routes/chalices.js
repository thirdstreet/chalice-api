/**
 * Created by damonfloyd on 5/28/15.
 */
var express = require('express');
var chaliceRouter = express.Router();
var dbrunner = require('../dbrunner');
var dm = require('../lib/datamassager');

chaliceRouter.use(function logreq(req, res, next) {
    console.log('Chalices:', req.method, req.url, req.hostname);
    next();
});

chaliceRouter.get('/', function(req, res) {
    var sql = 'CALL get_chalices(1)';
    dbrunner.run(sql, [], function(err, data) {
        if(err) {
	    console.log(err);
            res.status(500).json(err);
        }
        else {
            var chalices = dm.flattenChalices(data);
            res.status(200).json(chalices);
        }
    });
});

chaliceRouter.get('/root', function(req, res) {
    var sql = 'CALL get_chalices(0)';
    dbrunner.run(sql, [], function(err, data) {
        if(err) {
            res.status(500).json(err);
        }
        else {
            var chalices = dm.flattenChalices(data);
            res.status(200).json(chalices);
        }
    });
});

// get chalices which have glyphs
chaliceRouter.get('/glyph', function(req, res) {
    var sql = 'CALL get_chalices_with_glyphs()';
    dbrunner.run(sql, [], function(err, data) {
        if(err) {
            res.status(500).json(err);
        }
        else {
            var chalices = dm.flattenChalices(data);
            res.status(200).json(chalices);
        }
    });
});

chaliceRouter.get('/:Id', function(req, res) {
    dbrunner.run('CALL get_chalice(?)', [req.params.Id], function(err, data) {
        if(err) {
            res.status(500).json(err);
        }
        else {
            var chalices = dm.flattenChalices(data);
            res.status(200).json(chalices);
        }
    })
});

chaliceRouter.get('/area/:Id', function(req, res) {
    dbrunner.run('CALL get_area_chalices(?)', [req.params.Id], function(err, data) {
        if(err) {
            res.status(500).json(err);
        }
        else {
            var chalices = dm.flattenChalices(data);
            res.status(200).json(chalices);
        }
    });
});

chaliceRouter.post('/materials', function (req, res) {
    //console.log(req.body);
    if (!req.body.hasOwnProperty('mat1') || !req.body.hasOwnProperty('mat1qty')) {
        res.status(400).json({message: "You must supply at least 1 material and 1 quantity"});
    }
    else {
        var sql = 'CALL find_chalices_by_materials(?, ?, ?, ?, ?, ?, ?, ?)';
        var matsArray = [];
        matsArray.push(parseInt(req.body.mat1));
        matsArray.push(parseInt(req.body.mat1qty));
        if (req.body.hasOwnProperty('mat2') && req.body.hasOwnProperty('mat2qty')) {
            //sql += ', ?, ?';
            matsArray.push(parseInt(req.body.mat2));
            matsArray.push(parseInt(req.body.mat2qty));
        }
        else {
            matsArray.push(undefined);
            matsArray.push(undefined);
        }

        if (req.body.hasOwnProperty('mat3') && req.body.hasOwnProperty('mat3qty')) {
            //sql += ', ?, ?';
            matsArray.push(parseInt(req.body.mat3));
            matsArray.push(parseInt(req.body.mat3qty));
        }
        else {
            matsArray.push(undefined);
            matsArray.push(undefined);
        }

        if (req.body.hasOwnProperty('mat4') && req.body.hasOwnProperty('mat4qty')) {
            //sql += ', ?, ?';
            matsArray.push(parseInt(req.body.mat4));
            matsArray.push(parseInt(req.body.mat4qty));
        }
        else {
            matsArray.push(undefined);
            matsArray.push(undefined);
        }

        //sql = dbrunner.format(sql, matsArray);
        //res.status(200).json(sql);


        dbrunner.run(sql, matsArray, function (err, data) {
            if (err) {
                res.status(500).json({result: {error: err}});
                return;
            }
            else {
                res.status(200).json(dm.massage(data));
            }
        });

    }
});

module.exports = chaliceRouter;
