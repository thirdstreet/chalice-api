/**
 * Created by damonfloyd on 5/27/15.
 */
var express = require('express');
var glyphRouter = express.Router();
var dbrunner = require('../dbrunner');
var dm = require('../lib/datamassager');

glyphRouter.use(function logreq(req, res, next) {
    console.log('Glyphs:', req.method, req.url, req.hostname);
    next();
});

// 5 most recent
glyphRouter.get('/recent', function (req, res) {
    //console.log('GET recent');
    var sql = 'CALL get_recent_glyphs()';
    dbrunner.run(sql, function (err, data) {
        //console.log('hi...');
        if (err) {
            //console.log(err);
            res.status(500).json(err);
        }
        else {
            //console.log(data);
            res.status(200).json(dm.flattenGlyphs(data));
        }
    });
});

glyphRouter.post('/submitter', function (req, res) {
    var submitter = req.body.submitter;
    var sql = "CALL get_glyphs_by_submitter(?)";
    var args = [];
    args.push(submitter);
    dbrunner.run(sql, args, function (err, data) {
        if (err) {
            //console.log(err);
            res.status(500).json(err);
        }
        else {
            //console.log(data);
            res.status(200).json(dm.flattenGlyphs(data));
        }
    });
});

glyphRouter.post('/feature', function (req, res) {
    //console.log('POST to /feature');
    var sql = 'CALL find_glyphs_with_features(?,?,?,?)';
    var featArray = [];
    featArray.push(parseInt(req.body.feat1));
    if (req.body.hasOwnProperty('feat2')) {
        featArray.push(parseInt(req.body.feat2));
        if (req.body.hasOwnProperty('feat3')) {
            featArray.push(parseInt(req.body.feat3));
            if (req.body.hasOwnProperty('feat4')) {
                featArray.push(parseInt(req.body.feat4));
            }
            else {
                featArray.push(undefined);
            }
        }
        else {
            featArray.push(undefined, undefined);
        }
    }
    else {
        featArray.push(undefined, undefined, undefined);
    }

    dbrunner.run(sql, featArray, function (err, data) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(dm.flattenGlyphs(data));
        }
    })

});

// add a feature to a glyph
glyphRouter.post('/:Id/feature', function (req, res) {
    //console.log('POST to add a feature');
    var glyphId = req.params.Id;
    var featureId = req.body.featureId;
    var layer = req.body.layer;
    var notes = req.body.notes;
    var editPass = req.body.editPass;
    var args = [];
    args.push(glyphId, featureId, layer, notes, editPass);
    var sql = 'CALL add_glyph_feature(?, ?, ?, ?, ?)';
    dbrunner.run(sql, args, function (err, data) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    });
});

glyphRouter.get('/:Id/boss', function(req, res) {
    var glyphId = req.params.Id;
    var args = [];
    args.push(glyphId);
    var sql = 'CALL get_glyph_bosses(?)';
    dbrunner.run(sql, args, function(err, data) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    });
});

// add a boss to a glyph
glyphRouter.post('/:Id/boss', function (req, res) {
    //console.log('POST to add a layer boss');
    var glyphId = req.params.Id;
    var bossId = req.body.bossId;
    var layer = req.body.layer;
    var notes = req.body.notes;
    var editPass = req.body.editPass;
    var args = [];
    args.push(glyphId, bossId, layer, notes, editPass);
    var sql = 'CALL add_glyph_boss(?, ?, ?, ?, ?)';
    dbrunner.run(sql, args, function (err, data) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    });
});

glyphRouter.post('/boss', function (req, res) {
    //console.log('POST to /feature');
    var sql = 'CALL find_glyphs_with_bosses(?,?,?,?)';
    var bossArray = [];
    bossArray.push(parseInt(req.body.boss1));
    if (req.body.hasOwnProperty('boss2')) {
        bossArray.push(parseInt(req.body.boss2));
        if (req.body.hasOwnProperty('boss3')) {
            bossArray.push(parseInt(req.body.boss3));
            if (req.body.hasOwnProperty('boss4')) {
                bossArray.push(parseInt(req.body.boss4));
            }
            else {
                bossArray.push(undefined);
            }
        }
        else {
            bossArray.push(undefined, undefined);
        }
    }
    else {
        bossArray.push(undefined, undefined, undefined);
    }

    dbrunner.run(sql, bossArray, function (err, data) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(dm.flattenGlyphs(data));
        }
    })

});


// add a glyph
glyphRouter.post('/', function (req, res) {
    //console.log('POST to add');
    var chaliceId = req.body.chaliceId;
    var glyph = req.body.glyph;
    var isFetid = req.body.isFetid;
    var isRotted = req.body.isRotted;
    var isCursed = req.body.isCursed;
    var isSinister = req.body.isSinister;
    var submittedBy = req.body.submittedBy;
    var notes = req.body.notes;
    var editPass = req.body.editPass;

    var args = [];
    args.push(chaliceId, glyph, isFetid, isRotted, isCursed, isSinister, submittedBy, notes, editPass);
    var sql = 'CALL add_chalice_glyph(?, ?, ?, ?, ?, ?, ?, ?, ?)';
    dbrunner.run(sql, args, function (err, data) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    });
});

// remove a glyph (and all its features)
glyphRouter.delete('/:Id', function (req, res) {
    //console.log('DELETE by id')
    var sql = 'CALL delete_glyph(?, ?)';
    var glyphId = req.params.Id;
    var editPass = req.body.editPass;
    var args = [];
    args.push(glyphId, editPass);
    dbrunner.run(sql, args, function (err, data) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    })
});

glyphRouter.put('/:Id/:vote', function (req, res) {
    var sql = 'CALL add_glyph_vote(?, ?)';
    var glyphId = req.params.Id;
    var vote = req.params.vote;
    var args = [];
    args.push(glyphId, vote);
    dbrunner.run(sql, args, function (err, data) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    });
});

glyphRouter.put('/:Id', function (req, res) {
    var sql = 'CALL update_glyph(?, ?, ?, ?, ?, ?, ?, ?, ?)';
    var glyphId = req.params.Id;
    console.log(glyphId);
    var chaliceId = req.body.chaliceId;
    var glyph = req.body.glyph;
    var fetid = req.body.isFetid;
    var rotted = req.body.isRotted;
    var cursed = req.body.isCursed;
    var sinister = req.body.isSinister;
    var submitter = req.body.submitter;
    var editPass = req.body.editPass;

    var args = [];
    args.push(glyphId, chaliceId, glyph, fetid, rotted, cursed, sinister, submitter, editPass);
    dbrunner.run(sql, args, function (err, data) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(data);
        }
    });
});

glyphRouter.get('/', function (req, res) {
    //console.log('GET all');
    var sql = "CALL get_all_glyphs()";
    dbrunner.run(sql, function (err, data) {
        if (err) {
            res.status(500).json({result: {error: err}});
            return;
        }

        //res.status(200).json(data);
        var glyphs = dm.flattenGlyphs(data);
        res.status(200).json(glyphs);
    });
});

glyphRouter.get('/:id', function (req, res) {
    //console.log('GET by id');
    var sql = "CALL get_glyph(?)";
    dbrunner.run(sql, [req.params.id], function (err, data) {
        if (err) {
            res.status(500).json({result: {error: err}});
            return;
        }

        var glyph = dm.flattenGlyphs(data)[0];
        res.status(200).json({glyph: glyph});
    });
});


glyphRouter.get('/feature/:featureId', function (req, res) {
    //console.log('GET by FeatureId');
    var sql = "CALL get_glyphs_by_feature(?)";
    dbrunner.run(sql, [req.params.featureId], function (err, data) {
        if (err) {
            res.status(500).json({result: {error: err}});
            return;
        }
        var glyphs = dm.flattenGlyphs(data);
        res.status(200).json(glyphs);
    });
});

// glyphs by chalice
glyphRouter.get('/chalice/:Id', function (req, res) {
    //console.log('GET by ChaliceId');
    var sql = 'CALL get_glyphs_by_chalice(?)';
    var chaliceId = req.params.Id;
    dbrunner.run(sql, [chaliceId], function (err, data) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            res.status(200).json(dm.flattenGlyphs(data));
        }
    });
});


module.exports = glyphRouter;
