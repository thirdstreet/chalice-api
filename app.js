// LIBS
var express = require('express');
var router = express.Router();
var http = require('http');
var pkgjson = require('./package.json');
var bodyParser = require('body-parser');

var fs = require('fs');
var util = require('util');
var log_file = fs.createWriteStream(__dirname + '/tmp/debug.log', {flags: 'w'});
var log_stdout = process.stdout;

console.log = function (d) { //
    //log_file.write(util.format(d) + '\n');
    log_stdout.write(util.format(d) + '\n');
};

var cors = require('cors');

// the big kahuna
var app = express();
var dbrunner = require('./dbrunner').create();

app.set('port', 40704);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cors());

// setup for JWT stuff
app.use(function (req, res, next) {
    //res.setHeader('Access-Control-Allow-Origin', '*');
    //res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    //res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

app.use(cors());

router.use(function logreq(req, res, next) {
    console.log(req.method, req.url, req.hostname);
    next();
});

/****************************
 * Routes                    *
 ****************************/

// include all our routes
var glyphs = require('./routes/glyphs');
app.use('/glyph', glyphs);

var chalices = require('./routes/chalices');
app.use('/chalice', chalices);

var features = require('./routes/features');
app.use('/feature', features);

var bosses = require('./routes/bosses');
app.use('/boss', bosses);

router.get('/', function (req, res) {
    res.json({"version": pkgjson.version});
});


// this is so the app will actually you know...USE the router...
app.use('/', router);

/*****************************
 * End Routes
 *****************************/


// annnnnnd do the thing...
http.createServer(app).listen(app.get('port'), function () {
    console.log("server listening on " + app.get('port'));
});

